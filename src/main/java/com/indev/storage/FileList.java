package com.indev.storage;

/**
 * Created by ivan on 23.08.17.
 */
public class FileList {

    // относительный путь
    private String path;

    // перечнь файлов размещенных по запрошенному пути
    private String[] files;


    /**
     * Конструктор
     * @param path относительный путь
     */
    public FileList(String path)
    {
        this.setPath(path);
    }

    /**
     * Конструктор
     * @param path относительный путь
     * @param files файлы размещенные по этому относительному пути
     */
    public FileList(String path, String[] files )
    {
        this.setPath(path);
        this.setFiles(files);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String[] getFiles() {
        return files;
    }

    public void setFiles(String[] files) {
        this.files = files;
    }


}
