package com.indev.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "storage", name = "storage")
public class StorageController {

    private static final Logger Log = LoggerFactory.getLogger(StorageController.class);


    private final StorageService storageService;

    @Autowired
    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    /**
     * Просмотр содержимого каталога
     *
     * @param path путь по которому надо просмотреть файлы
     * @return Перечень файлов
     */
    @GetMapping(path = "list", name = "list")
    public FileList listFiles(@RequestParam(name = "path", defaultValue = "") String path) {
        Log.info("StorageController.listFiles: path={}", path);
        try {
            List<Path> pathList = storageService.listItem(path);
            List<String> stringPathList = new ArrayList<String>();
            for (Path itemPathList : pathList)
                stringPathList.add(itemPathList.toString());

            return new FileList(path, stringPathList.toArray(new String[stringPathList.size()]));

        } catch (Exception ex) {
            Log.error("StorageController.listFiles: path={}\n", path, ex);
            throw new StorageFileNotFoundException("FileUploadController.listFiles: Не могу обнаружеть указанный путь", ex);
        }
    }
    
    /**
     * Выгрузить файл на клиент
     *
     * @param filename - имя файла с относительным путем
     * @return Пример выгрузки файла
     * http://localhost:8080/storage/serve?file=abc/123.txt
     * или
     * curl -O -X GET http://localhost:8080/storage/serve?file=abc/123.txt
     */
    @GetMapping(path = "serve", name = "serve")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@RequestParam("file") String filename) throws IOException {
        Log.info("StorageController.serveFile: filename={}", filename);
        Resource file = storageService.serveItemAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    /**
     * Загрузить файл на сервер
     *
     * @param file               - загружаемый файл
     * @param relativePath       - относительный путь, где следует разместить файл
     * @param redirectAttributes
     * @return Пример загрузки файла с консоли
     * curl -i -X POST \
     * -F "file=@/Users/ivan/temp.forupload/2015.Форма отчета руководителя департамента.docx" http://localhost:8080/storage/store
     * или
     * curl -i -X POST -F "relativepath=abc" \
     * -F "file=@/Users/ivan/temp.forupload/2015.Форма отчета руководителя департамента.docx" http://localhost:8080/storage/store
     */
    @PostMapping(path = "store", name = "store")
    public String storeFile(@RequestParam("file") MultipartFile file,
                            @RequestParam(name = "relativepath", defaultValue = "") String relativePath,
                            RedirectAttributes redirectAttributes) {

// DONE сделать загрузку файлов https://indevpro.atlassian.net/browse/TRAD-47

        storageService.storeItem(file, relativePath, true);

        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/";
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}
