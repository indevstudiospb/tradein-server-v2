package com.indev.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface StorageService {

    /**
     * Инициализировать хранилище
     */
    void init();

    /**
     * Сохранить файл в хранилище по умолчанию
     * @param file файл для размещения
     */
    void storeItem(MultipartFile file);

    /**
     * Сохранить файл в хранилище
     * @param file файл для размещения.
     * @param relativePath относительный путь.
     * @param useRelativePath использовать или нет относительный путь при размещении.
     * @param storagePath - путь к хранилищу, если null, то используется по умолчанию.
     */
    void storeItem(MultipartFile file, String relativePath, boolean useRelativePath, String storagePath);

    /**
     * Сохранить файл в хранилище по умолчанию
     * @param file файл для размещения.
     * @param relativePath относительный путь.
     * @param useRelativePath использовать или нет относительный путь при размещении.
     */
    void storeItem(MultipartFile file, String relativePath, boolean useRelativePath );

    /**
     * Получить перечень содержимого
     *
     * @param filename путь, содержимое которого хотим получить
     * @return
     */
    List<Path> listItem(String filename);

//    @Deprecated
//    Path load(String filename);

    /**
     * Выгрузить файл
     * @param filename относительнельный путь с именем файла
     * @return
     */
    Resource serveItemAsResource(String filename) throws IOException;

    Resource serveItemAsResource(String filename, String storagePath) throws IOException;

    /**
     * Удалить все содержимое
     */
    @Deprecated
    void deleteAll();

    /**
     * Удалить файл
     *
     * @param filename
     */
    @Deprecated
    void deleteItem(String filename);

    /**
     * Переместить файл в хранилище
     * @param sourceFilename
     * @param targetFilename
     */
    @Deprecated
    void moveItem(String sourceFilename, String targetFilename);

    void storeObject(String targetRelativeFileName, Object objectToStore, boolean appendFlag, String storagePath) throws IOException;

    void storeObject(String targetRelativeFileName, Object objectToStore, boolean appendFlag) throws IOException;
}
