package com.indev.storage;

import com.indev.tradein.entity.ProjectAX;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileSystemStorageService implements StorageService {

    private static final Logger Log = LoggerFactory.getLogger(FileSystemStorageService.class);


    private final Path rootLocation;

    private StorageProperties properties;

    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.properties = properties;
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public void storeItem(MultipartFile file, String relativePath, boolean useRelativePath, String storagePath) {
        Log.info("FileSystemStorageService.storeItem file={} relativePath={} useRelativePath={} storagePath={}",
                (null == file) ? "null" : file.getOriginalFilename(),
                (null == relativePath) ? "null" : relativePath,
                useRelativePath, storagePath);

        String filename = StringUtils.cleanPath(file.getOriginalFilename());

        Path destantionPath = (null == storagePath) ? this.rootLocation : Paths.get(storagePath);

        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            // проверка на безопасность
            securityCheck(filename, "filename");
            if (useRelativePath) {
                if (null == relativePath)
                    throw new IllegalArgumentException("FileSystemStorageService.store.relativePath is null");
                relativePath = StringUtils.cleanPath(relativePath);
                securityCheck(relativePath, "relativePath");
                destantionPath = destantionPath.resolve(relativePath);
            }

            // создать путь если он отсутствет
            Files.createDirectories(destantionPath);

            // формируем полное имя целевого файла
            Path destantionFile = destantionPath.resolve(filename);

            // проверка наличия файла до копирования
            if (!Files.notExists(destantionFile)) {
                Log.warn("FileSystemStorageService.storeItem file {} already exists. OVERWRITING.", destantionFile);
            }

            // скопировать файл
            Files.copy(file.getInputStream(), destantionFile,
                    StandardCopyOption.REPLACE_EXISTING);


        } catch (IOException ex) {
            Log.error("FileSystemStorageService.storeItem file={} relativePath={} useRelativePath={} storagePath={}\n",
                    (null == file) ? "null" : file.getOriginalFilename(),
                    (null == relativePath) ? "null" : relativePath,
                    useRelativePath, storagePath, ex);
            throw new StorageException("Failed to store file " + filename, ex);
        }
    }

    @Override
    public void storeItem(MultipartFile file, String relativePath, boolean useRelativePath) {
        storeItem(file, relativePath, useRelativePath, null);
    }

    @Override
    public List<Path> listItem(String filename) {
        try {
            // проверка на безопасность
            securityCheck(filename, "filename");

            Path targetPath = this.rootLocation.resolve(filename);
            List<Path> listPath = new ArrayList<>();

            File dirScaner = new File(targetPath.toString());
            if (dirScaner.isDirectory()) {
                String[] fileList = dirScaner.list();
                for (String currentFile : fileList) {
                    if (!Files.isDirectory(Paths.get(targetPath.toString(), currentFile)))
                        listPath.add(Paths.get(currentFile));
                }
                return listPath;
            } else {
                Log.error("FileSystemStorageService.listItem: The path " + targetPath + " must be directory");
                throw new StorageException("The path " + targetPath + " must be directory");
            }

        } catch (Exception e) {
            Log.error("FileSystemStorageService.listItem: Failed to read stored files at {}", filename, e);
            throw new StorageException("Failed to read stored files at " + filename, e);
        }
    }

    @Override
    public Resource serveItemAsResource(String filename) throws IOException {
        return serveItemAsResource(filename, null);
    }

    @Override
    public Resource serveItemAsResource(String filename, String storagePath) throws IOException {
        try {
            Log.info("FileSystemStorageService.serveItemAsResource file={} storagePath={}",
                    filename, storagePath);

            // проверка на безопасность
            securityCheck(filename, "filename");

            Path file = ( null == storagePath ) ? rootLocation.resolve(filename)
                    : Paths.get(storagePath).resolve(filename);

            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    @Deprecated
    public void deleteAll() {
        Log.error("FileSystemStorageService.deleteAll UnsupportedOperation {}" + rootLocation.toString());
        throw new UnsupportedOperationException("FileSystemStorageService.deleteAll UnsupportedOperation {}" + rootLocation.toString());
        // это оригинальная строка FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    // TODO public void deleteItem(String filename) {
    @Override
    @Deprecated
    public void deleteItem(String filename) {
        // проверка на безопасность
        securityCheck(filename, "filename");

    }

    // TODO public void moveItem(String sourceFilename, String targetFilename) {
    @Override
    @Deprecated
    public void moveItem(String sourceFilename, String targetFilename) {
        // проверка на безопасность
        securityCheck(sourceFilename, "sourceFilename");
        securityCheck(targetFilename, "targetFilename");

    }

    @Override
    public void storeObject(String targetRelativeFileName, Object objectToStore, boolean appendFlag, String storagePath) throws StorageException {
        Log.info("FileSystemStorageService:storeObject {}", targetRelativeFileName);
        if (null == targetRelativeFileName)
            throw new NullPointerException("FileSystemStorageService:storeObject targetRelativeFileName");
        if (null == objectToStore)
            throw new NullPointerException("FileSystemStorageService:storeObject objectToStore");
        // проверка что путь соответствует требованиям
        securityCheck(targetRelativeFileName, "targetRelativeFileName");

        Path destantionPath = (null == storagePath) ? this.rootLocation : Paths.get(storagePath);
        destantionPath = destantionPath.resolve(targetRelativeFileName);

        // создать путь если он отсутствет
        try {
            Files.createDirectories(destantionPath.getParent());
        } catch (IOException ex) {
            Log.error("FileSystemStorageService:storeObject", ex);
            throw new StorageException("FileSystemStorageService:storeObject can't createDirectories " + destantionPath.toString(), ex);
        }

        try (FileWriter fileWriter = new FileWriter(destantionPath.toString(), appendFlag)) {
            fileWriter.write(objectToStore.toString());
        } catch (IOException ex) {
            Log.error("FileSystemStorageService:storeObject", ex);
            throw new StorageException("FileSystemStorageService:storeObject can't write " + destantionPath.toString(), ex);
        }
    }

    @Override
    public void storeObject(String targetRelativeFileName, Object objectToStore, boolean appendFlag) throws IOException {
        storeObject(targetRelativeFileName, objectToStore, appendFlag, null);
    }


    /**
     * Проверка пути на требования безопасности.
     * Путь должен быть относительным.
     *
     * @param filename  путь, который подлежит проверке.
     * @param paramName имя параметра
     */
    private void securityCheck(String filename, String paramName) {
        // проверка на безопасность
        String param = (paramName != null) ? paramName : "[null]";

        if (filename.contains(".."))
            // This is a security check
            throw new StorageException(
                    "Cannot process file with relative path outside current directory "
                            + filename + " parameter " + param);
    }

    @Override
    public void init() {
        Log.info("FileSystemStorageService:init storage={}", rootLocation);
        try {
            if (properties.isClearOnStart())
                deleteAll();
            // создать путь целевого хранилища
            Files.createDirectories(rootLocation);
            // установить для проекта путь к хранилищу по умолчанию
            ProjectAX.DefineGlobalPath(rootLocation.toString());

        } catch (IOException ex) {
            Log.error("FileSystemStorageService:init storage={}", rootLocation, ex);
            throw new StorageException("Could not initialize storage", ex);
        }
    }

    @Override
    public void storeItem(MultipartFile file) {
        this.storeItem(file, null, false, null);
    }
}
