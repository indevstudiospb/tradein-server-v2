package com.indev.system;

/**
 * Created by ivan on 18.08.17.
 */
public class MemoryInfo {

    private long totalMemoryMB = 0;

    private long freeMemoryMB = 0;

    private static final int MB = (1024*1024);

    public MemoryInfo()
    {
        update();
    }

    public void update() {
        this.totalMemoryMB = Runtime.getRuntime().totalMemory() / MB;
        this.freeMemoryMB = Runtime.getRuntime().freeMemory() / MB;
    }


    public long getTotalMemoryMB() {
        return totalMemoryMB;
    }

    public long getFreeMemoryMB() {
        return freeMemoryMB;
    }
}
