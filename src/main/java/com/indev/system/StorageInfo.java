package com.indev.system;

import lombok.Getter;

import java.io.File;

/**
 * Информация о хранилище
 */

public class StorageInfo {

    private File storageFile = null;

    @Getter
    private long totalSpaceGB = 0;

    @Getter
    private long freeSpaceGB = 0;

    @Getter
    private long usableSpaceGB = 0;

    @Getter
    private String path;

    public StorageInfo(final String path)
    {
        if(null == path )
            throw new IllegalArgumentException("_path can't be null");

        this.path = path;

        storageFile = new File(path);

        update();

    }

    private static final int GB = (1024*1024*1024);

    public void update()
    {
        this.totalSpaceGB = storageFile.getTotalSpace()/GB;
        this.freeSpaceGB = storageFile.getFreeSpace()/GB;
        this.usableSpaceGB = storageFile.getUsableSpace()/GB;
    }
}
