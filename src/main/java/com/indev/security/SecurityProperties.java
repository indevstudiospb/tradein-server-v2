package com.indev.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("security")
public class SecurityProperties {

    @Value("${security.tablet.pass}")
    private String tabletPass;
    @Value("${security.axapta.pass}")
    private String axaptaPass;
    @Value("${security.extsystem.pass}")
    private String extsystemPass;

    public String getTabletPass() {
        return tabletPass;
    }

    public String getAxaptaPass() {
        return axaptaPass;
    }

    public String getExtsystemPass() {
        return extsystemPass;
    }
}
