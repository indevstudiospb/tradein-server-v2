package com.indev.security;

public interface Roles {

    // Префикс "ROLE_"  у названий ролей обязателен!

    String TABLET_PC = "ROLE_TABLETPC";
    String AXAPTA = "ROLE_AXAPTA";
    String EXT_SYSTEM = "ROLE_EXTSYSTEM";
}
