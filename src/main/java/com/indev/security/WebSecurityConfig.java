package com.indev.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.savedrequest.NullRequestCache;


@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private SecurityProperties properties;

    @Autowired
    public WebSecurityConfig(SecurityProperties props) {
        properties = props;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //.antMatchers("/","/about")
                //.permitAll()//
                .anyRequest().authenticated() //
                .and().requestCache().requestCache(new NullRequestCache()) //
                .and().httpBasic() //
                .and().csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/about");
    }

    @Autowired
    void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication() //
                .withUser("tablet_pc").password(properties.getTabletPass()).authorities(Roles.TABLET_PC) //
                .and() //
                .withUser("axapta").password(properties.getAxaptaPass()).authorities(Roles.AXAPTA)
                .and()
                .withUser("ext_system").password(properties.getExtsystemPass()).authorities(Roles.EXT_SYSTEM);
    }
}
