package com.indev.test;

/**
 * Created by ivan on 22.08.17.
 */
@Deprecated
public class TestContainer {
    public String key = "test_key";
    public String value ="test_value";

    /**
     * Тестовый контейнер ключ значение
     */
    public TestContainer() {
        this("key","value");
    }

    /**
     * Тестовый контейнер ключ значение
     * @param key - ключ
     * @param value - значение
     */
    public TestContainer(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
