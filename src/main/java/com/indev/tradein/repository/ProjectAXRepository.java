package com.indev.tradein.repository;

import com.indev.tradein.entity.AXAPTAID;
import com.indev.tradein.entity.ProjectAX;
import com.indev.tradein.entity.ProjectAxShortDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ivan on 22.08.17.
 */
@Repository
public interface ProjectAXRepository extends CrudRepository<ProjectAX, Long> {

    ProjectAX findByAxaptaId(@Param("axapta_id") AXAPTAID axaptaId);

    ProjectAX findByActivationId(@Param("activation_id") String activationId);

    ProjectAX findByActivationIdAndActive(@Param("activation_id") String activationId,
                                          @Param("active") Boolean active);

    @Query(nativeQuery = true, value = "select * from project order by project_id")
    Iterable<ProjectAX> findAllOrderById();

}
