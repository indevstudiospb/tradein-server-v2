package com.indev.tradein.repository;

import com.indev.tradein.entity.BoughtPhone;
import com.indev.tradein.entity.ProjectAX;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Created by ivan on 31.08.17.
 */
@Repository
public interface BoughtPhoneRepository extends CrudRepository<BoughtPhone, Long> {

    BoughtPhone findFirstByImei1AndProjectAxaptaAndActiveAndAxaptaIdIsNullOrderByTransactionTimeDesc(
            @Param("imei1") String imei1,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active);

    BoughtPhone findFirstByImei2AndProjectAxaptaAndActiveAndAxaptaIdIsNullOrderByTransactionTimeDesc(
            @Param("imei2") String imei2,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active);


    BoughtPhone findFirstByImei1AndProjectAxaptaAndActiveAndDeliveryOrderByTransactionTimeDesc(
            @Param("imei1") String imei1,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active,
            @Param("delivery") Boolean delivery);

    BoughtPhone findFirstByImei2AndProjectAxaptaAndActiveAndDeliveryOrderByTransactionTimeDesc(
            @Param("imei2") String imei2,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active,
            @Param("delivery") Boolean delivery);

    Set<BoughtPhone> findAllByProjectAxaptaAndActiveAndImei1In(@Param("project_id") ProjectAX projectAX,
                                                               @Param("active") Boolean active,
                                                               @Param("imeiSet") Set<String> imeiSet);

}
