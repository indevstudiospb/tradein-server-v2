package com.indev.tradein.repository;

import com.indev.tradein.entity.FileDescription;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ivan on 31.08.17.
 */
@Repository
public interface FileDescriptionRepository extends CrudRepository<FileDescription, Long>{
    
}
