package com.indev.tradein.repository;

import com.indev.tradein.entity.AXAPTAID;
import com.indev.tradein.entity.ProjectAX;
import com.indev.tradein.entity.TabletPC;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ivan on 22.08.17.
 */
@Repository
@Deprecated
public interface TabletPCRepository extends CrudRepository<TabletPC, Long> {

    TabletPC findByAxaptaId(@Param("axapta_id") AXAPTAID axaptaId);

    List<TabletPC> findAllByProjectAxaptaIdAndActiveIsTrue(@Param("project_id") ProjectAX projectAxaptaId);

    TabletPC findByActivationId(@Param("activation_id") String activationId);

}