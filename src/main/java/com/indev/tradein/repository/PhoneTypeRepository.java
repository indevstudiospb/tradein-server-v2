package com.indev.tradein.repository;

import com.indev.tradein.entity.AXAPTAID;
import com.indev.tradein.entity.PhoneType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by ivan on 31.08.17.
 */
@Repository
public interface PhoneTypeRepository extends CrudRepository<PhoneType, Long>{

    PhoneType findByAxaptaId(@Param("axapta_id") AXAPTAID axaptaId);

    PhoneType findByModel(@Param("model") String model);
    
    Set<PhoneType> findAllByAxaptaIdIn(@Param("axapta_id_set")Set<AXAPTAID> axaptaIdSet);
    
    List<PhoneType> findByActive(Boolean active);

}
