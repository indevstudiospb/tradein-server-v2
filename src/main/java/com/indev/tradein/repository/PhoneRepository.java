package com.indev.tradein.repository;

import com.indev.tradein.entity.Phone;
import com.indev.tradein.entity.PhoneType;
import com.indev.tradein.entity.ProjectAX;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by ivan on 31.08.17.
 */
@Repository
public interface PhoneRepository extends CrudRepository<Phone, Long> {

//    Phone findByImei1(@Param("imei1") String imei1);

    Phone findByProjectAxaptaAndImei1AndPhoneTypeAndActive(
            @Param("project_id") ProjectAX projectAX,
            @Param("imei1") String imei1,
            @Param("phone_type") PhoneType phoneType,
            @Param("active") Boolean active);

//    Phone findByImei1AndProjectAxaptaAndActiveOrderByProductionTimeDesc(@Param("imei1") String imei1,
//                                                                        @Param("project_id") ProjectAX projectAX,
//                                                                        @Param("active") Boolean active);
//
//    Phone findByImei2AndProjectAxaptaAndActiveOrderByProductionTimeDesc(@Param("imei2") String imei2,
//                                                                        @Param("project_id") ProjectAX projectAX,
//                                                                        @Param("active") Boolean active);

    Phone findByImei1AndProjectAxaptaAndActiveOrderByProductionTimeDesc(
            @Param("imei1") String imei1,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active);

    Phone findByImei2AndProjectAxaptaAndActiveOrderByProductionTimeDesc(
            @Param("imei2") String imei2,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active);

    Phone findFirstByImei1AndProjectAxaptaAndActiveOrderByProductionTimeDesc(
            @Param("imei1") String imei1,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active);

    Phone findFirstByImei1AndProjectAxaptaOrderByProductionTimeDesc(
            @Param("imei1") String imei1,
            @Param("project_id") ProjectAX projectAX);

    Phone findFirstByImei2AndProjectAxaptaAndActiveOrderByProductionTimeDesc(
            @Param("imei2") String imei2,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active);

    Phone findFirstByImei2AndProjectAxaptaOrderByProductionTimeDesc(
            @Param("imei2") String imei2,
            @Param("project_id") ProjectAX projectAX);

    List<Phone> findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneIsNull(@Param("project_id") ProjectAX projectAX,
                                                                                        @Param("from") Timestamp from,
                                                                                        @Param("to") Timestamp to,
                                                                                        @Param("active") Boolean active);


    List<Phone> findAllByProjectAxaptaAndUpdateTimeBetweenAndActiveAndBoughtPhoneIsNull(@Param("project_id") ProjectAX projectAX,
                                                                                        @Param("from") Timestamp from,
                                                                                        @Param("to") Timestamp to,
                                                                                        @Param("active") Boolean active);


    List<Phone> findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneNotNull(@Param("project_id") ProjectAX projectAX,
                                                                                         @Param("from") Timestamp from,
                                                                                         @Param("to") Timestamp to,
                                                                                         @Param("active") Boolean active);

    List<Phone> findAllByProjectAxaptaAndUpdateTimeBetweenAndActiveAndBoughtPhoneNotNull(@Param("project_id") ProjectAX projectAX,
                                                                                         @Param("from") Timestamp from,
                                                                                         @Param("to") Timestamp to,
                                                                                         @Param("active") Boolean active);

    List<Phone> findAllByProjectAxaptaAndCreateTimeBetweenAndActive(@Param("project_id") ProjectAX projectAX,
                                                                    @Param("from") Timestamp from,
                                                                    @Param("to") Timestamp to,
                                                                    @Param("active") Boolean active);


    List<Phone> findAllByProjectAxaptaAndUpdateTimeBetweenAndActive(@Param("project_id") ProjectAX projectAX,
                                                                    @Param("from") Timestamp from,
                                                                    @Param("to") Timestamp to,
                                                                    @Param("active") Boolean active);


    List<Phone> findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneIsNullAndPhoneType(@Param("project_id") ProjectAX projectAX,
                                                                                                    @Param("from") Timestamp from,
                                                                                                    @Param("to") Timestamp to,
                                                                                                    @Param("active") Boolean active,
                                                                                                    @Param("phone_type_id") PhoneType phoneType);


    List<Phone> findAllByProjectAxaptaAndUpdateTimeBetweenAndActiveAndBoughtPhoneIsNullAndPhoneType(@Param("project_id") ProjectAX projectAX,
                                                                                                    @Param("from") Timestamp from,
                                                                                                    @Param("to") Timestamp to,
                                                                                                    @Param("active") Boolean active,
                                                                                                    @Param("phone_type_id") PhoneType phoneType);


    List<Phone> findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneNotNullAndPhoneType(@Param("project_id") ProjectAX projectAX,
                                                                                                     @Param("from") Timestamp from,
                                                                                                     @Param("to") Timestamp to,
                                                                                                     @Param("active") Boolean active,
                                                                                                     @Param("phone_type_id") PhoneType phoneType);


    List<Phone> findAllByProjectAxaptaAndUpdateTimeBetweenAndActiveAndBoughtPhoneNotNullAndPhoneType(@Param("project_id") ProjectAX projectAX,
                                                                                                     @Param("from") Timestamp from,
                                                                                                     @Param("to") Timestamp to,
                                                                                                     @Param("active") Boolean active,
                                                                                                     @Param("phone_type_id") PhoneType phoneType);


    List<Phone> findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndPhoneType(@Param("project_id") ProjectAX projectAX,
                                                                                @Param("from") Timestamp from,
                                                                                @Param("to") Timestamp to,
                                                                                @Param("active") Boolean active,
                                                                                @Param("phone_type_id") PhoneType phoneType);
    List<Phone> findAllByProjectAxaptaAndUpdateTimeBetweenAndActiveAndPhoneType(@Param("project_id") ProjectAX projectAX,
                                                                                @Param("from") Timestamp from,
                                                                                @Param("to") Timestamp to,
                                                                                @Param("active") Boolean active,
                                                                                @Param("phone_type_id") PhoneType phoneType);
}
