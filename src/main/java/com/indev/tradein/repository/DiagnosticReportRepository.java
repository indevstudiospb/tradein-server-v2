package com.indev.tradein.repository;

import com.indev.tradein.entity.AXAPTAID;
import com.indev.tradein.entity.DiagnosticReport;
import com.indev.tradein.entity.Phone;
import com.indev.tradein.entity.ProjectAX;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by ivan on 31.08.17.
 */
@Repository
public interface DiagnosticReportRepository extends CrudRepository<DiagnosticReport, Long> {

    DiagnosticReport findByAxaptaId(@Param("axapta_id") AXAPTAID axaptaId);

    DiagnosticReport findByCheckSessionId(@Param("check_session_id") String checkSessionId);

    // сортировка выборки по времени, вернуть самый свежий диагностический отчет
    DiagnosticReport findFirstByPhoneAndProjectAxaptaAndActiveOrderByCreateTimeDesc(
            @Param("phone_id") Phone phone,
            @Param("project_id") ProjectAX projectAX,
            @Param("active") Boolean active);

    // Выбрать самый последний отчет, выполненный по данному телефону (телефон на данный момент уже выбран по imei и проекту)
    DiagnosticReport findFirstByPhoneOrderByCreateTimeDesc(@Param("phone_id") Phone phone);

    // Выбрать все отчеты по проекту в интервале времени
    List<DiagnosticReport> findAllByProjectAxaptaAndCreateTimeBetweenOrderByCreateTimeDesc(ProjectAX project, Timestamp from, Timestamp to);

}
