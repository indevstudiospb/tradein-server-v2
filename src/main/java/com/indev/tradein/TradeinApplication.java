package com.indev.tradein;

import com.indev.security.SecurityProperties;
import com.indev.storage.StorageProperties;
import com.indev.storage.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.TimeZone;

@SpringBootApplication(scanBasePackages = {"com.indev.storage", "com.indev.tradein", "com.indev.security"})
@EnableConfigurationProperties({StorageProperties.class, SecurityProperties.class, ProcessProperties.class})
@SpringBootConfiguration
public class TradeinApplication {

    private static final Logger Log = LoggerFactory.getLogger(TradeinApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(TradeinApplication.class, args);
    }

    @Autowired
    private TimestampCustomDeserializer customDeserializer;

    @Autowired
    private TimestampCustomSerializer customSerializer;

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer addCustomBigDecimalDeserialization() {
        return new Jackson2ObjectMapperBuilderCustomizer() {

            @Override
            public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
                jacksonObjectMapperBuilder.deserializerByType(Timestamp.class, customDeserializer);
                jacksonObjectMapperBuilder.serializerByType(Timestamp.class, customSerializer);
            }
        };
    }


    @PostConstruct
    void started() {
        Log.info("TradeinApplication: set TimeZone to UTC");
        TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
    }

    @Bean()
//  @DependsOn("name")
    @Autowired
    CommandLineRunner init(StorageProperties storageProperties, StorageService storageService) {
        try {
            Log.info("TradeinApplication: init() {}", storageProperties.getLocation());
            return (args) -> {
                // инициализация хранилища
                storageService.init();
            };

        } catch (Exception ex) {
            Log.error("The StorageService can't initialize.", ex);
            throw ex;
        }
    }

}
