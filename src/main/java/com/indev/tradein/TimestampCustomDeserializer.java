package com.indev.tradein;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Timestamp;

@Component
public class TimestampCustomDeserializer extends StdDeserializer<Timestamp> {

    public TimestampCustomDeserializer() {
        super(Timestamp.class);
    }

    private DateTimeFormatter formatter = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss.SSSZ");

    @Override
    public Timestamp deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        DateTime dt = DateTime.parse(p.getText(), this.formatter);
        return new Timestamp(dt.getMillis());
    }
}
