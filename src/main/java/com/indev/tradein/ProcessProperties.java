package com.indev.tradein;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ToString
@ConfigurationProperties("process")
public class ProcessProperties {
    @Setter
    @Getter
    @Value("${process.unknown}")
    private String unknownAxaptaId;

    @Setter
    @Getter
    @Value("${process.apk.version}")
    private String apkVersion;

    @Setter
    @Getter
    @Value("${process.api.version}")
    private String apiVersion;
    
}
