package com.indev.tradein.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.indev.security.Roles;
import com.indev.storage.StorageService;
import com.indev.tradein.ProcessProperties;
import com.indev.tradein.entity.*;
import com.indev.tradein.repository.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("report")
public class DiagnosticReportController {

    private final ProjectAXRepository projectAXRepository;

    private final DiagnosticReportRepository diagnosticReportRepository;

//    private final TabletPCRepository tabletPCRepository;

    private final PhoneTypeRepository phoneTypeRepository;

    private final PhoneRepository phoneRepository;

    private final TabletPCRepository tabletPCRepository;

    private final StorageService storageService;

    private final ProcessProperties processProperties;

    @Autowired
    private ObjectMapper objectMapper;

    private static final Logger Log = LoggerFactory.getLogger(DiagnosticReportController.class);

    @Autowired
    DiagnosticReportController(DiagnosticReportRepository diagnosticReportRepository,
                               ProjectAXRepository projectAXRepository,
                               PhoneTypeRepository phoneTypeRepository,
                               PhoneRepository phoneRepository,
                               TabletPCRepository tabletPCRepository,
                               StorageService storageService,
                               ProcessProperties processProperties) {

        this.diagnosticReportRepository = diagnosticReportRepository;
        this.projectAXRepository = projectAXRepository;
        this.phoneTypeRepository = phoneTypeRepository;
        this.phoneRepository = phoneRepository;
        this.tabletPCRepository = tabletPCRepository;
        this.storageService = storageService;
        this.processProperties = processProperties;

        Log.info("DiagnosticReportController.processProperties={}\n", processProperties);
    }

    //<editor-fold desc="EXCEPTION HANDLER RINE">
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleException(RuntimeException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleException(IllegalArgumentException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleException(NullPointerException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleException(AccessDeniedException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }
    //</editor-fold>


    @Secured({Roles.TABLET_PC})
    @PostMapping(path = "test", name = "test")
    public ResponseEntity<?> test(MultipartRequest request, @RequestParam(name = "report", defaultValue = "") String report,
                                  @RequestParam(name = "diagnostic", defaultValue = "") String diagnostic) throws IOException {

        DiagnosticReport diagnosticReport = null;

        // десерилизуем объект из строки
        try {
            diagnosticReport = objectMapper.readValue(report, DiagnosticReport.class);
        } catch (IOException ex) {
            Log.warn("store(): Can't create DiagnosticReport from string {}", report);
            throw new IllegalArgumentException("Can't create DiagnosticReport from string " + report, ex);
        }

        return new ResponseEntity<>(diagnosticReport, HttpStatus.OK);
    }

    /**
     * Сохранение отчета.
     * Условие: в отчете указываем либо планшет либо логин
     *
     * @param request
     * @param report
     * @param diagnostic
     * @return
     * @throws IOException
     */
    @Secured({Roles.TABLET_PC})
    @PostMapping(path = "store", name = "store")
    @Transactional
    public ResponseEntity<?> store(MultipartRequest request, @RequestParam(name = "report", defaultValue = "") String report,
                                   @RequestParam(name = "diagnostic", defaultValue = "") String diagnostic,
                                   @RequestParam(name = "checkSessionId", required = false) String checkSessionId) throws IOException {
        Log.info("DiagnosticReportController.store report={}", report);
        // проверяем параметр, что нам на вход дали json отчет
        if (report == null || report.isEmpty())
            throw new IllegalArgumentException("report can't be empty");

        // проверяем параметр, что нам на вход дали json отчет
        if (diagnostic == null || diagnostic.isEmpty())
            throw new IllegalArgumentException("diagnostic can't be empty");

        DiagnosticReport diagnosticReport = null;
        Map diagnosticReportMap = null;

        // десерилизуем объект из строки
        try {

            diagnosticReport = objectMapper.readValue(report, DiagnosticReport.class);
            diagnosticReportMap = objectMapper.readValue(report, Map.class);
            diagnosticReport.setJsonPhoneDiagnosticReport(diagnostic);

        } catch (IOException ex) {
            Log.warn("store(): Can't create DiagnosticReport from string {}", report);
            throw new IllegalArgumentException("Can't create DiagnosticReport from string " + report, ex);
        }

        // TODO реализовать логику корректной простановки контроля рейтинга аппарата и простановку соответствующего признака активности результата

        // принудительно сбрасываем значения идентификатора
        diagnosticReport.setId(0);
        diagnosticReport.setAxaptaId(null);

        ProjectAX prj;
        // TODO отключить прием протоколов в рамках неактивных проектов
        if (diagnosticReport.getProjectAxapta() != null && diagnosticReport.getProjectAxapta().getActivationId() != null) {
            // вторая версия - без планшета, в отчете сразу получаем activationId проекта
            prj = projectAXRepository.findByActivationIdAndActive(diagnosticReport.getProjectAxapta().getActivationId(), true);
            if (null == prj || 0 == prj.getId()) {
                Log.error("DiagnosticReportController.store: ProjectAX {} not found or inactive", report);
                throw new RuntimeException("ProjectAX " + diagnosticReport.getProjectAxapta() + " not found or inactive. Report:" + report);
            }

        } else {
            // первая версия с планшетом. нам надо сначала из планшета понять, что это за проект
            // определяем к какому проекту относится планшет
            String tabletActivationId = findTabletActivationId(diagnosticReportMap);
            if (tabletActivationId == null)
                throw new IllegalArgumentException("Unknown TabletPC " + report);

            TabletPC tabletPC = tabletPCRepository.findByActivationId(tabletActivationId);
            if (null == tabletPC || 0 == tabletPC.getId()) {
                throw new IllegalArgumentException("Unknown TabletPC " + report);
            }

            if (null == tabletPC.getProjectAxaptaId()) {
                // Планшет не привязан к проекту
                Log.warn("DiagnosticReportController.store: TabletPC must be linked with Project, report=\n{}", report);
                throw new RuntimeException("DiagnosticReportController.store: TabletPC must be linked with Project report=\n" + report);
            }

            prj = projectAXRepository.findOne(tabletPC.getProjectAxaptaId().getId());
            if (null == prj || 0 == prj.getId()) {
                Log.error("DiagnosticReportController.store: Unknown ProjectAX {}", report);
                throw new RuntimeException("Unknown ProjectAX " + tabletPC.getId() + " report:" + report);
            }
        }

        String imei1 = diagnosticReport.getPhone().getImei1();
        if (null == imei1 || imei1.isEmpty()) {
            Log.error("DiagnosticReportController.store: Invalid IMEI1 {}", report);
            throw new RuntimeException("DiagnosticReportController.store: Invalid IMEI1. report:" + report);
        }

        // простановка соответствия типа телефона
        PhoneType phoneType = null;
        for (PhoneType type : prj.getPhoneTypes()) {
            if (type.getId() == diagnosticReport.getPhone().getPhoneType().getId())
                if (type.getActive() == true) {
                    // проставляем тип только если совпадает идентификатор типа и проставлен признак активности
                    phoneType = type;
                    break;
                }
        }

        if (null == phoneType || 0 == phoneType.getId()) {
            Log.warn("DiagnosticReportController.store: Unknown PhoneType {}", report);
            // присваиваем тип телефона UNKNOWN
            phoneType = getUnknownPhoneType();
        }

        // корректировка IMEI для тестового эмулятора android - автоматическое формирование imei
//        if (phoneType.getModel().contains("Android SDK") || diagnosticReport.getPhone().getModelCaption().contains("Android SDK")) {
//            imei1 = UUID.randomUUID().toString();
//            Log.warn("DiagnosticReportController.store: change imei1 to={}", imei1);
//            diagnosticReport.getPhone().setImei1(imei1);
//        }
        if (diagnosticReport.getPhone().getImei1().equals("000000000000000")) {
            imei1 = UUID.randomUUID().toString();
            Log.warn("DiagnosticReportController.store: change imei1 from '000000000000000' to={}", imei1);
            diagnosticReport.getPhone().setImei1(imei1);
        }

        // проверяем регистрировали или нет ранее телефон указанного типа в рамках данного проекта
        // TODO надо или нет блокировать аппараты, которые найдены в системе, но признак активности у них проставлен disable
        Phone phone = phoneRepository.findByProjectAxaptaAndImei1AndPhoneTypeAndActive(prj, imei1, phoneType, true);
        // активного телефона не нашли, ищем неактивный аппарат
        phone = (null != phone) ? phone : phoneRepository.findByProjectAxaptaAndImei1AndPhoneTypeAndActive(prj, imei1, phoneType, false);

        if (null != phone) {
            // мы нашли, данный аппарат уже проходил через систему
            // это тот же самый аппарат, устанавливаем его в отчете
            Log.info("DiagnosticReportController.store: Phone already exists report:\n{}", phone);
            diagnosticReport.setPhone(phone);
        }

        // заполняем проект
        diagnosticReport.setProjectAxapta(prj);
        diagnosticReport.getPhone().setPhoneType(phoneType);
        diagnosticReport.getPhone().setProjectAxapta(prj);

        // расставляем признаки активности
        applyActivityLogic(diagnosticReport);

        // время создания отчета - ставим текущее время
        diagnosticReport.setCreateTime(new Timestamp((new java.util.Date()).getTime()));

        // это пережиток времен планшета
        // если время отчета не задано, то пытаемся брать его из структуры phone
//        if (null == diagnosticReport.getCreateTime())
//            diagnosticReport.setCreateTime(diagnosticReport.getPhone().getCreateTime());
//        if (null == diagnosticReport.getCreateTime())
//            diagnosticReport.setCreateTime(diagnosticReport.getPhone().getProductionTime());

        if (checkSessionId != null && !checkSessionId.isEmpty())
            diagnosticReport.setCheckSessionId(checkSessionId);

        diagnosticReport = diagnosticReportRepository.save(diagnosticReport);

//        DateTime createDate = new DateTime(diagnosticReport.getPhone().getProductionTime());
        DateTime createDate = new DateTime(diagnosticReport.getCreateTime());
        DateTimeFormatter fmtDate = DateTimeFormat.forPattern("yyyyMMdd");

        // путь проектного хранилища
        String storagePath = prj.getStoragePath();

        String relativePath = prj.getId() + "/" + createDate.toString(fmtDate) + "/" + diagnosticReport.getId() + "/";

        // корректировка пути хранилища проекта
        // сохранение файлов
        for (Map.Entry<String, MultipartFile> item : request.getFileMap().entrySet()) {
            Log.debug(item.getKey());
            FileDescription fd = new FileDescription();
            fd.setFileName(item.getValue().getOriginalFilename());
            fd.setFileSize(item.getValue().getSize());
            fd.setDiagnosticReportId(diagnosticReport.getId());
            fd.setActive(diagnosticReport.getActive());
            diagnosticReport.getFileDescription().add(fd);
            storageService.storeItem(item.getValue(), relativePath, true, storagePath);
        }
        // #TRAD-79 сохранение исходных параметров report и diagnostic в файл
        storageService.storeObject(relativePath + "/report.json", report, false, storagePath);
        storageService.storeObject(relativePath + "/diagnostic.json", diagnostic, false, storagePath);

        // добавить в Phone ссылку на текущий диагностический отчет
        diagnosticReport.getPhone().setDiagnosticReportId(diagnosticReport.getId());
        diagnosticReport.getPhone().setQualityRate(diagnosticReport.getQualityRateEx());

        // обновить таблицу описания файлов
        diagnosticReport = diagnosticReportRepository.save(diagnosticReport);

        return new ResponseEntity<>(new DiagnosticReportBrief(diagnosticReport), HttpStatus.OK);
    }

    // Расставить признаки активности
    private void applyActivityLogic(DiagnosticReport report) {

        Phone phone = report.getPhone();

        if (!phone.getPhoneType().getActive() || !report.getProjectAxapta().getActive()) {
            // планшет, тип телефона или проект неактивны, деактивируем отчет и телефон
            Log.warn("DiagnosticReportController.store tabletpc, phonetype or project was disabled, disable phone and report={}",
                    report);
            report.setActive(false);
            phone.setActive(false);
        } else {
            report.setActive(true);
            phone.setActive(true);
        }

    }

    private String findTabletActivationId(Map diagnosticReportMap) {
        String result = null;
        Object tablet = diagnosticReportMap.get("tabletPC");
        if (tablet != null)
        {
            Object activId = ((Map)tablet).get("activationId");
            if (activId != null)
                result = (String) activId;
        }
        return result;
    }

    private PhoneType getUnknownPhoneType() {
        PhoneType phoneType;
        phoneType = phoneTypeRepository.findByModel("UNKNOWN");
        if (null == phoneType) {
            // надо автоматически создать поле тип модели UNKNOWN
            PhoneType unknownPhoneType = new PhoneType();
            unknownPhoneType.setModel("UNKNOWN");
            unknownPhoneType.setActive(false);
            unknownPhoneType.setVendorName("UNKNOWN");
            // получаем идентификатор axaptaId для неизвестного типа телефона
            String unknownAxaptaId = processProperties.getUnknownAxaptaId();
            unknownAxaptaId = (null == unknownAxaptaId) ? "ZZZZZZZZZZ" : unknownAxaptaId;
            unknownPhoneType.setAxaptaId(new AXAPTAID(unknownAxaptaId));
            phoneType = phoneTypeRepository.save(unknownPhoneType);
        }
        if (null == phoneType)
            throw new RuntimeException("DiagnosticReportController.getUnknownPhoneType: Can't create PhoneType UNKNOWN");

        return phoneType;
    }

    /**
     * Прочитать отчет. Допустимо указать один из идентификаторов отчёта (id или axaptaId).
     *
     * @param axaptaId идентификатор axapta, должен соответствовать требованиям.
     * @param id       идентификатор по базе.
     * @return Если данные по идентификатору обнаружены, то возвращается OK(200, "OK") и json с данными.
     * Если данные по идентификатору не обнаружены, то возвращаем
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") и пустую json структуру ProjectAX.
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     */
    @Transactional(readOnly = true)
    @Secured({Roles.AXAPTA})
    @GetMapping(name = "read", path = "read")
    public ResponseEntity<?> read
    (@RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,  // axaptaId of the report
     @RequestParam(name = "id", defaultValue = "0") Long id,                // id of the report
     @RequestParam(name = "file", required = false) String imageFileName,   // file in the report
     @RequestParam(name = "command", required = false, defaultValue = "") String command,   // command (getreport)
     @RequestParam(required = false) String checkSessionId) throws IOException {
        try {
            Log.info("DiagnosticReportController.read id={} axaptaId={} checkSessionId={} command={} file={} ", id, axaptaId, checkSessionId, command, imageFileName);

            DiagnosticReport report = null;

            // сначала пытаемся достать отчет по его идентификатору, если он задан
            if (id != 0) {
                // задан Id базы данных
                report = diagnosticReportRepository.findOne(id);
            } else if (null != axaptaId && axaptaId.isValid()) {
                // задан axaptaId отчета
                report = diagnosticReportRepository.findByAxaptaId(axaptaId);
            } else if (checkSessionId != null) {
                // задан checkSessionId
                report = diagnosticReportRepository.findByCheckSessionId(checkSessionId);
            }

            if (null == report)
                return new ResponseEntity<>(new DiagnosticReport(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

            switch (command) {
                case "getreport":
                    return new ResponseEntity<>(report.getJsonPhoneDiagnosticReport(), HttpStatus.OK);

                case "getimage":
                    return onCommandGetImage(report, imageFileName);

                case "":
                    // команда не задана
                    break;
                // какая-то другая неверная команда
                default: {
                    Log.error("TabletPCController.read id={} axaptaid={} command={}. Unknown command", id, axaptaId, command);
                    throw new IllegalArgumentException("TabletPCController.read Unknown command=" + command);
                }
            }


            return new ResponseEntity<>(report, HttpStatus.OK);

        } catch (Exception ex) {
            Log.error("DiagnosticReportController.read id={} axaptaId={} command={}\n", id, axaptaId, command, ex);
            throw ex;
        }
    }

    /**
     * Прочитать отчет.
     *
     * @param checkSessionId идентификатор
     * @param command        getreport.
     * @return Если данные по идентификатору обнаружены, то возвращается OK(200, "OK") и json с данными.
     * Если данные по идентификатору не обнаружены, то возвращаем
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") и пустую json структуру ProjectAX.
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     */
    @Transactional(readOnly = true)
    @Secured({Roles.AXAPTA, Roles.EXT_SYSTEM})
    @GetMapping(name = "readExt", path = "readExt")
    public ResponseEntity<?> readExt
    (@RequestParam(required = false) String checkSessionId,
     @RequestParam(name = "command", required = false, defaultValue = "") String command)   // command (getreport)
            throws IOException {
        try {
            Log.info("DiagnosticReportController.read checkSessionId={} command={}", checkSessionId, command);

            DiagnosticReport report = null;

            if (checkSessionId != null) {
                report = diagnosticReportRepository.findByCheckSessionId(checkSessionId);
            }

            if (null == report)
                return new ResponseEntity<>(new DiagnosticReport(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

            switch (command) {
                case "getreport":
                    return new ResponseEntity<>(report.getJsonPhoneDiagnosticReport(), HttpStatus.OK);
                case "":
                    // команда не задана
                    break;
                // какая-то другая неверная команда
                default: {
                    Log.error("TabletPCController.read checkSessionId={} command={}. Unknown command", checkSessionId, command);
                    throw new IllegalArgumentException("TabletPCController.read Unknown command=" + command);
                }
            }


            return new ResponseEntity<>(report, HttpStatus.OK);

        } catch (Exception ex) {
            Log.error("DiagnosticReportController.read checkSessionId={} command={}\n", checkSessionId, command, ex);
            throw ex;
        }
    }

    private ResponseEntity<?> onCommandGetImage(DiagnosticReport report, String imageFileName) throws IOException {

        if (null != report && null != imageFileName) {

            ProjectAX prj = report.getProjectAxapta();
            if (prj == null)
                throw new NullPointerException("DiagnosticReportController.onCommandGetLogo project is null, report.id="
                        + report.getId());

            DateTime timeStamp = new DateTime(report.getCreateTime());
            DateTimeFormatter fmtDate = DateTimeFormat.forPattern("yyyyMMdd");

            // путь проектного хранилища
            String storagePath = prj.getStoragePath();

//            String relativePath = prj.getId() + "/" + timeStamp.toString(fmtDate) + "/" + tabletPC.getId()
//                    + "/" + report.getId() + "/";

            String relativePath = prj.getId() + "/" + timeStamp.toString(fmtDate) + "/" + report.getId() + "/";

            List<FileDescription> imageList = report.getFileDescription();
            if (null != imageList)
                for (FileDescription imageFile :
                        imageList) {
                    if (imageFile.getFileName().toLowerCase().equals(imageFileName.toLowerCase())) {
                        // корректировка пути хранилища проекта
                        // сохранение файлов
                        String relativeFileName = relativePath + imageFileName;

                        Resource file = storageService.serveItemAsResource(relativeFileName, storagePath);

                        HttpHeaders headers = new HttpHeaders();
                        headers.setContentType(MediaType.IMAGE_JPEG);
                        return new ResponseEntity<>(file, headers, HttpStatus.OK);
                    }
                }
            return new ResponseEntity<>(report, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(report, HttpStatus.BAD_REQUEST);

    }

    /**
     * Запрос списка отчетов по проекту в интервале времени. Для идентификации проекта достаточно задать один из ид.
     *
     * @param axaptaId      идентификатор проекта по axapta, должен соответствовать требованиям.
     * @param activationId  идентификатор проекта.
     * @param fromTimeStamp начало интервала
     * @param toTimeStamp   конец интервала
     * @return Если данные по запросу обнаружены, то возвращается OK(200, "OK") и json с данными.
     * Если данные по идентификатору не обнаружены, то возвращаем
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") и пустое тело.
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     */
    @Secured({Roles.AXAPTA})
    @GetMapping(name = "readAll", path = "readAll")
    public ResponseEntity<?> readAll(
            @RequestParam(name = "activationId", required = false) String activationId,
            @RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,
            @RequestParam(name = "from") Timestamp fromTimeStamp,
            @RequestParam(name = "to") Timestamp toTimeStamp) throws Exception {
        Log.info("PhoneController.readAll: activationId={} axaptaId={} from={} to={}}",
                activationId, (null == axaptaId) ? "null" : axaptaId, fromTimeStamp, toTimeStamp);

        try {

            ProjectAX projectAX = null;
            // проверка входных параметров
            if (null != axaptaId && axaptaId.isValid()) {
                // задан AxaptaId
                projectAX = projectAXRepository.findByAxaptaId(axaptaId);
            } else if (null != activationId) {
                // задан activationId
                projectAX = projectAXRepository.findByActivationIdAndActive(activationId, true);
            }

            if (null == projectAX)
                throw new IllegalArgumentException("PhoneController.readAll: Project not found or inactive. activationId=" + activationId + " axaptaId=" + axaptaId);

            List<DiagnosticReport> list =
                    diagnosticReportRepository.findAllByProjectAxaptaAndCreateTimeBetweenOrderByCreateTimeDesc(
                            projectAX, fromTimeStamp, toTimeStamp);

            if (list == null || list.isEmpty())
                return ResponseEntity.status(416).build();
            else
                return ResponseEntity.ok(list);

        } catch (Exception ex) {
            Log.error("PhoneController.readAll: axaptaId={} activationId={} from={} to={}\n",
                    axaptaId, activationId, fromTimeStamp, toTimeStamp, ex);
            throw ex;
        }
    }

}
