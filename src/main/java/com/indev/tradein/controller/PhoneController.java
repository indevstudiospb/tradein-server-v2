package com.indev.tradein.controller;

import com.indev.security.Roles;
import com.indev.storage.StorageService;
import com.indev.tradein.entity.*;
import com.indev.tradein.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.Random;
import java.util.Set;

/**
 * REST контроллер ресурса телефон и выкупленный телефон Phone и BoughtPhone
 */
@RestController
@RequestMapping("phone")
public class PhoneController {

    private final PhoneTypeRepository phoneTypeRepository;
    private final PhoneRepository phoneRepository;
    private final BoughtPhoneRepository boughtPhoneRepository;
    private final DiagnosticReportRepository diagnosticReportRepository;
    private final ProjectAXRepository projectAXRepository;


    private final StorageService storageService;

    @PersistenceContext
    EntityManager entityManager;

    private static final Logger Log = LoggerFactory.getLogger(PhoneController.class);

    @Autowired
    PhoneController(PhoneTypeRepository phoneTypeRepository,
                    DiagnosticReportRepository diagnosticReportRepository,
                    PhoneRepository phoneRepository,
                    BoughtPhoneRepository boughtPhoneRepository,
                    ProjectAXRepository projectAXRepository,
                    StorageService storageService) {
        this.phoneTypeRepository = phoneTypeRepository;
        this.diagnosticReportRepository = diagnosticReportRepository;
        this.boughtPhoneRepository = boughtPhoneRepository;
        this.phoneRepository = phoneRepository;
        this.storageService = storageService;
        this.projectAXRepository = projectAXRepository;
    }

    //<editor-fold desc="EXCEPTION HANDLER RINE">
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleException(RuntimeException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleException(IllegalArgumentException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleException(NullPointerException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleException(AccessDeniedException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }
    //</editor-fold>

    @Secured({Roles.EXT_SYSTEM})
    @GetMapping(name = "buy", path = "buy")
    @Transactional
    public ResponseEntity<?> buy(
            @RequestParam(name = "activationId", required = true) String uuidProject,
            @RequestParam(name = "imei", required = true) String imei,
            @RequestParam(name = "command", required = false) String command) {
        Log.info("PhoneController.buy activationId={} imei={}", uuidProject, imei);

        try {
            if (!checkCommand(command))
                throw new IllegalArgumentException("PhoneController.buy: invalid argument command. activationId=" + uuidProject
                        + " imei=" + imei + " command=" + command);

            // проверка входных параметров
            if (0 == imei.length())
                throw new IllegalArgumentException("PhoneController.buy: invalid argument imei. activationId=" + uuidProject
                        + " imei=" + imei);

            // проверяем валидность UUID проекта
            ProjectAX projectAX = projectAXRepository.findByActivationIdAndActive(uuidProject, true);
            if (null == projectAX)
                throw new IllegalArgumentException("PhoneController.buy: active Project isn't founded. activationId=" + uuidProject
                        + " imei=" + imei);

            // ATTENTION!!! мы полагаемся что IMEI уникальный инчае, надо при выкупе указывать тип телефона

            // делаем поиск активного аппарата по imei среди телефонов, которые прошли проверку и в рамках указанного проекта
            // проверка что аппарат оценивался в рамках целевого проекта
            Phone phone = phoneRepository.findFirstByImei1AndProjectAxaptaAndActiveOrderByProductionTimeDesc(imei, projectAX, true);
            phone = (phone == null) ? (phoneRepository.findFirstByImei2AndProjectAxaptaAndActiveOrderByProductionTimeDesc(imei, projectAX, true)) : phone;
            if (null == phone) {
                Log.warn("PhoneController.buy: phone isn't found activationId={} imei={}", uuidProject, imei);
                return new ResponseEntity<>("PhoneController.buy: phone isn't found imei=" + imei, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
            }
            // TODO убедиться что повторный выкуп телефона возможен после цикла восстановления в рамках этого проекта
            // проверяем, что активная запись с выкупленным телефоном еще не создавалась или она не обработана в центре обработки
            BoughtPhone buyPhone = (null != phone.getBoughtPhone()) ? phone.getBoughtPhone()
                    : boughtPhoneRepository.findFirstByImei1AndProjectAxaptaAndActiveAndAxaptaIdIsNullOrderByTransactionTimeDesc
                    (imei, projectAX, true);
            buyPhone = (null != buyPhone) ? buyPhone
                    : boughtPhoneRepository.findFirstByImei2AndProjectAxaptaAndActiveAndAxaptaIdIsNullOrderByTransactionTimeDesc
                    (imei, projectAX, true);

            if (null != buyPhone || (null != command && command.toLowerCase().equals("cancel"))) {
                if (null != command && command.toLowerCase().equals("cancel")) {
                    if (null == buyPhone) {
                        // телефон для отмены выкупа не обнаружен
                        Log.warn("PhoneController.buy: Nothing to cancel. BoughtPhone isn't found activationId={} imei={}", uuidProject, imei);
                        return new ResponseEntity<>("PhoneController.buy: Nothing to cancel. BoughtPhone isn't found imei=" +
                                imei, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
                    }
                    // отмена покупки
                    Log.info("PhoneController.buy.Cancel: BoughtPhone id={}. activationId={} imei={}\n{}",
                            buyPhone.getId(), uuidProject, imei, buyPhone);
                    if (null != buyPhone.getDelivery() && true == buyPhone.getDelivery()) {
                        // отменять выкуп нельзя, так как данный телефон уже в доставке
                        Log.warn("PhoneController.buy.Cancel: BoughtPhone already delivered id={}. activationId={} imei={}\n{}",
                                buyPhone.getId(), uuidProject, imei, buyPhone);

                        return new ResponseEntity<>(new BoughtPhoneBrief(buyPhone), HttpStatus.PAYMENT_REQUIRED);
                    }
                    // анулируем связь с выкупленным телефоном
                    phone.setBoughtPhone(null);
                    // дезактивируем запись о выкупе
                    buyPhone.setActive(false);
                    buyPhone.setImei1(String.format("%06d.%s", (short) (new Random()).nextInt(1 << 15),
                            (null == buyPhone.getImei1()) ? ("undefined") : (buyPhone.getImei1())));
                    phoneRepository.save(phone);
                    buyPhone = boughtPhoneRepository.save(buyPhone);
                    return new ResponseEntity<>(new BoughtPhoneBrief(buyPhone), HttpStatus.OK);

                } else {
                    // выкупленный телефон уже создан, повторного его не создаем
                    Log.warn("PhoneController.buy: BoughtPhone already exists id={}. activationId={} imei={}\n{}",
                            buyPhone.getId(), uuidProject, imei, buyPhone);
                    return new ResponseEntity<>(new BoughtPhoneBrief(buyPhone), HttpStatus.ALREADY_REPORTED);
                }
            }

            // проверяем наличие всех признаков active, проверяем что PhoneType активен
            PhoneType phoneType = phone.getPhoneType();
            if (!phoneType.getActive()) {
                // выкуп аппарата с данным типом запрещен
                Log.warn("PhoneController.buy: PhoneType isn't active projectid={}, phonetype={}\nphone={}\n",
                        projectAX.getAxaptaId(), phoneType, phone);
                // выкуп данной модели аппарата запрещен
                return new ResponseEntity<>(phoneType, HttpStatus.NOT_ACCEPTABLE);
            }


            // проверяем что разрешен выкуп для определенного при диагностики рейтинга качества
            String phonePrice = null;
            try {
                phonePrice = FindPriceByQuality(QUALITY.valueOf(phone.getQualityRate()), phoneType);
            } catch (Exception ex) {
                Log.warn("PhoneController.buy: Can't map quality to price projectid={}, phonetype={}\nphone={}\n",
                        projectAX.getAxaptaId(),
                        phoneType, phone, ex);
                throw new RuntimeException(ex);
            }
            if (null == phonePrice || 0 == phonePrice.length()) {
                // выкуп данной модели аппарата запрещен
                return new ResponseEntity<>(phoneType, HttpStatus.NOT_ACCEPTABLE);
            }

            // создаем запись с выкупленным телефоном
            buyPhone = new BoughtPhone(phone);
            buyPhone.setacceptedPrice(phonePrice);
            // TODO проверить корректность времени пока заполнил бредом надо убедиться что UTC
            buyPhone.setTransactionTime(new Timestamp((new java.util.Date()).getTime()));
            buyPhone.setActive(true);
            if (null != command && command.toLowerCase().equals("getprice")) {
                // не применяем изменений и возвращаем цену
                entityManager.detach(buyPhone);
                return new ResponseEntity<>(new BoughtPhoneBrief(buyPhone), HttpStatus.OK);
            }

            // проставляем ссылку на выкупленный телефон в Phone
            phone.setBoughtPhone(buyPhone);
            // сохраняем Phone и по каскаду BoughtPhone
            phone = phoneRepository.save(phone);

            return new ResponseEntity<>(new BoughtPhoneBrief(phone.getBoughtPhone()), HttpStatus.CREATED);


        } catch (RuntimeException ex) {
            Log.error("PhoneController.buy activationId={} imei={}\n", uuidProject, imei, ex);
            throw ex;
        }
    }

    private boolean checkCommand(String command) {
        if (null == command)
            return true;
        else if (command.toLowerCase().equals("getprice"))
            return true;
        else if (command.toLowerCase().equals("cancel"))
            return true;
        return false;
    }

    private String FindPriceByQuality(QUALITY quality, PhoneType phoneType) throws Exception {
        if (null == phoneType)
            throw new NullPointerException("PhoneController.FindPriceByQuality phoneType");

        String price = null;

        switch (quality) {
            case rate1:
                price = phoneType.getRate1Price();
                break;
            case rate2:
                price = phoneType.getRate2Price();
                break;
            case rate3:
                price = phoneType.getRate3Price();
                break;
            case rate4:
                price = phoneType.getRate4Price();
                break;
            case rate5:
                price = phoneType.getRate5Price();
                break;
            case rate6:
                price = phoneType.getRate6Price();
                break;
            case rate7:
                price = phoneType.getRate7Price();
                break;
            case rate8:
                price = phoneType.getRate8Price();
                break;
            case rate9:
                price = phoneType.getRate9Price();
                break;
            case rate10:
                price = phoneType.getRate10Price();
                break;
            case rate11:
                price = phoneType.getRate11Price();
                break;
            case rate12:
                price = phoneType.getRate12Price();
                break;
            case rate13:
                price = phoneType.getRate13Price();
                break;
            case rate14:
                price = phoneType.getRate14Price();
                break;
            case rate15:
                price = phoneType.getRate15Price();
                break;
            case rate16:
                price = phoneType.getRate16Price();
                break;
            case rate17:
                price = phoneType.getRate17Price();
                break;
            case rate18:
                price = phoneType.getRate18Price();
                break;
            case rate19:
                price = phoneType.getRate19Price();
                break;
            case rate20:
                price = phoneType.getRate20Price();
                break;
            case normal:
                price = phoneType.getNormalRatePrice();
                break;
            case minor:
                price = phoneType.getMinorRatePrice();
                break;
            case major:
                price = phoneType.getMajorRatePrice();
                break;
            case blocker:
                price = phoneType.getBlockerRatePrice();
                break;
            default:
                Log.error("PhoneController.FindPriceByQuality: Unsupported quality type {}", quality);
                throw new Exception("PhoneController.FindPriceByQuality: Unsupported quality type " + quality);
        }
        if (null == price || 0 == price.length()) {
            // выкуп данной модели запрещен
            Log.warn("PhoneController.FindPriceByQuality Выкуп данной аппарата запрещен системой по рейтингу качества");
        }

        return price;
    }

    @Transactional
    @Secured({Roles.EXT_SYSTEM})
    @PostMapping(name = "delivery", path = "delivery")
    public ResponseEntity<?> delivery(@RequestParam(name = "activationId") String uuidProject,
                                      @RequestParam(name = "imeiSet") Set<String> imeiSet) {
        Log.info("PhoneController.delivery: activationId={} imeiSet={}", uuidProject, imeiSet);

        // проверяем валидность UUID проекта
        ProjectAX projectAX = projectAXRepository.findByActivationIdAndActive(uuidProject, true);
        if (null == projectAX)
            throw new IllegalArgumentException("PhoneController.delivery: active Project isn't founded. activationId=" + uuidProject);

        // TODO недо продумать как это будет соотноситься что в рамках проекта могут быть активные телефоны с одним и тем-же imei, может ограничить временным диапазоном
        Set<BoughtPhone> boughtPhones = boughtPhoneRepository.findAllByProjectAxaptaAndActiveAndImei1In(projectAX,
                true, imeiSet);

        if (boughtPhones == null || boughtPhones.size() != imeiSet.size()) {
            // выборка не совпадает с запросом
            return new ResponseEntity<>(boughtPhones, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
        }

        for (BoughtPhone bPhone : boughtPhones) {
            bPhone.setDelivery(true);
            boughtPhoneRepository.save(bPhone);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Запрос последнего отчета по телефону в проекте.
     * Вторая версия запроса, не учитывает значение поля active как для проекта, так и для отчета
     * @param uuidProject
     * @param imei
     * @param command
     * @return
     * @throws Exception
     */
    @Secured({Roles.AXAPTA, Roles.EXT_SYSTEM})
    @GetMapping(name = "read", path = "read")
    public ResponseEntity<?> read(
            @RequestParam(name = "activationId") String uuidProject,
            @RequestParam(name = "imei") String imei,
            @RequestParam(name = "command", required = false, defaultValue = "") String command) throws Exception {
        Log.info("PhoneController.read: activationId={} imei={} command={}", uuidProject, imei, command);

        try {

                ProjectAX projectAX = projectAXRepository.findByActivationId(uuidProject);
                if (null == projectAX)
                    throw new IllegalArgumentException("PhoneController.read: active Project isn't founded. activationId=" + uuidProject + " imei=" + imei);

                Phone phone = null;
                phone = phoneRepository.findFirstByImei1AndProjectAxaptaOrderByProductionTimeDesc(imei, projectAX);
                if (null == phone)
                    phone = phoneRepository.findFirstByImei2AndProjectAxaptaOrderByProductionTimeDesc(imei, projectAX);

                if (phone == null) {
                    Log.warn("PhoneController.read: phone isn't found activationId={} imei={}", uuidProject, imei);
                    return new ResponseEntity<>(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
                }

            DiagnosticReport diagnosticReport = diagnosticReportRepository.findFirstByPhoneOrderByCreateTimeDesc(phone);

            if (null == diagnosticReport) {
                // соответствующий диагностический отчет не найден
                Log.warn("PhoneController.read: DiagnosticReport isn't found. Повторите диагностику. activationId={} imei={} phone={}",
                        uuidProject, imei, phone);
                return new ResponseEntity<>(phone, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
            }

            if (command.equals("getreport"))
                return new ResponseEntity<>(diagnosticReport.getJsonPhoneDiagnosticReport(), HttpStatus.OK);

            return new ResponseEntity<>(new DiagnosticReportExternal(diagnosticReport,
                    FindPriceByQuality(QUALITY.valueOf(diagnosticReport.getPhone().getQualityRate()),
                            diagnosticReport.getPhone().getPhoneType())), HttpStatus.OK);

        } catch (Exception ex) {
            Log.error("PhoneController.read: activationId={} imei={}\n", uuidProject, imei, ex);
            throw ex;
        }
    }

    // Ванина версия запроса
//    @Secured({Roles.AXAPTA, Roles.EXT_SYSTEM})
//    @GetMapping(name = "read", path = "read")
//    public ResponseEntity<?> read(
//            @RequestParam(name = "activationId") String uuidProject,
//            @RequestParam(name = "imei") String imei,
//            @RequestParam(name = "command", required = false, defaultValue = "") String command) throws Exception {
//        Log.info("PhoneController.read: activationId={} imei={} command={}", uuidProject, imei, command);
//
//        try {
//            // проверяем валидность UUID проекта
//            ProjectAX projectAX = projectAXRepository.findByActivationIdAndActive(uuidProject, true);
//            if (null == projectAX)
//                throw new IllegalArgumentException("PhoneController.read: active Project isn't founded. activationId=" + uuidProject
//                        + " imei=" + imei);
//            // поиск целевого аппарата
//            Phone phone = null;
//
//            phone = phoneRepository.findFirstByImei1AndProjectAxaptaAndActiveOrderByProductionTimeDesc(imei, projectAX, true);
//
//            phone = (null == phone) ? phoneRepository.findFirstByImei2AndProjectAxaptaAndActiveOrderByProductionTimeDesc(imei, projectAX, true) : phone;
//
//            if (null == phone) {
//                Log.warn("PhoneController.read: phone isn't found activationId={} imei={}", uuidProject, imei);
//                return new ResponseEntity<>(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
//            }
//
//            // поиск диагностического отчета
//            DiagnosticReport diagnosticReport =
//                    (0 != phone.getDiagnosticReportId()) ?
//                            diagnosticReportRepository.findOne(phone.getDiagnosticReportId()) :
//                            diagnosticReportRepository.findFirstByPhoneAndProjectAxaptaAndActiveOrderByCreateTimeDesc(phone, projectAX, true);
//
//            if (null == diagnosticReport) {
//                // соответствующий диагностический отчет не найден
//                Log.warn("PhoneController.read: DiagnosticReport isn't found. Повторите диагностику. activationId={} imei={} phone={}",
//                        uuidProject, imei, phone);
//                return new ResponseEntity<>(phone, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
//            }
//
//            // TODO нужен метод выгрузки фото телефона в момент диагностики, метод сделан, но он у DiagnosticReportController
//            if (command.equals("getreport"))
//                return new ResponseEntity<>(diagnosticReport.getJsonPhoneDiagnosticReport(), HttpStatus.OK);
//
//            return new ResponseEntity<>(new DiagnosticReportExternal(diagnosticReport,
//                    FindPriceByQuality(QUALITY.valueOf(diagnosticReport.getPhone().getQualityRate()),
//                            diagnosticReport.getPhone().getPhoneType())), HttpStatus.OK);
//
//        } catch (Exception ex) {
//            Log.error("PhoneController.read: activationId={} imei={}\n", uuidProject, imei, ex);
//            throw ex;
//        }
//    }
//
    @Secured({Roles.AXAPTA})
    @GetMapping(name = "readAll", path = "readAll")
    public ResponseEntity<?> readAll(
            @RequestParam(name = "activationId", required = false) String activationId,
            @RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,
            @RequestParam(name = "from") Timestamp fromTimeStamp,
            @RequestParam(name = "to") Timestamp toTimeStamp,
            @RequestParam(name = "phoneTypeAxaptaId", required = false) AXAPTAID phoneTypeAxaptaId,
            @RequestParam(name = "active", defaultValue = "true") Boolean active,
            @RequestParam(name = "isBuy", required = false) Boolean isBuy) throws Exception {
        Log.info("PhoneController.readAll: activationId={} axaptaId={} from={} to={} phoneTypeAxaptaId={} isBuy={}",
                activationId, (null == axaptaId) ? "null" : axaptaId, fromTimeStamp, toTimeStamp, phoneTypeAxaptaId, isBuy);

        try {

            ProjectAX projectAX = null;
            // проверка входных параметров
            if (null != axaptaId && axaptaId.isValid()) {
                // задан AxaptaId
                projectAX = projectAXRepository.findByAxaptaId(axaptaId);
            } else if (null != activationId) {
                // задан activationId
                projectAX = projectAXRepository.findByActivationIdAndActive(activationId, true);
            } else {
                Log.warn("PhoneController.readAll: Undefined project activationId={} axaptaId={} from={} to={} phoneTypeAxaptaId={} isBuy={}",
                        activationId, (null == axaptaId) ? "null" : axaptaId, fromTimeStamp, toTimeStamp, phoneTypeAxaptaId, isBuy);
                return new ResponseEntity<>("PhoneController.readAll: Undefined project id. Set parameter activationId or axaptaId", HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
            }

            // проверяем валидность UUID проекта
            if (null == projectAX)
                throw new IllegalArgumentException("PhoneController.readAll: active Project isn't founded. activationId=" + activationId
                        + " phoneType=" + phoneTypeAxaptaId);

            if (null != phoneTypeAxaptaId && phoneTypeAxaptaId.isValid()) {
                // указан тип телефона для выборки
                PhoneType phoneType = phoneTypeRepository.findByAxaptaId(phoneTypeAxaptaId);
                Iterable<Phone> phoneList;
                if (null != phoneType) {
                    if (null == isBuy) {
                        phoneList = phoneRepository.findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndPhoneType(
                                projectAX, fromTimeStamp, toTimeStamp, active, phoneType);
                    } else if (true == isBuy) {
                        phoneList = phoneRepository.
                                findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneNotNullAndPhoneType(
                                        projectAX, fromTimeStamp, toTimeStamp, active, phoneType);

                    } else {
                        phoneList = phoneRepository.
                                findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneIsNullAndPhoneType(
                                        projectAX, fromTimeStamp, toTimeStamp, active, phoneType);

                    }

                    return new ResponseEntity<>(phoneList, HttpStatus.OK);
                }
                throw new IllegalArgumentException("PhoneController.readAll:Unknown PhoneType"
                        + " activationId=" + activationId
                        + " from=" + fromTimeStamp
                        + " to=" + toTimeStamp
                        + " phoneTypeAxaptaId=" + phoneType
                        + " isBuy=" + isBuy);
            } else {
                Iterable<Phone> phoneList;
                // не указан тип телефона для выборки
                if (null == isBuy) {
                    phoneList = phoneRepository.findAllByProjectAxaptaAndCreateTimeBetweenAndActive(
                            projectAX, fromTimeStamp, toTimeStamp, active);
                } else if (true == isBuy) {
                    phoneList = phoneRepository.
                            findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneNotNull(
                                    projectAX, fromTimeStamp, toTimeStamp, active);

                } else {
                    phoneList = phoneRepository.
                            findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneIsNull(
                                    projectAX, fromTimeStamp, toTimeStamp, active);

                }

                return new ResponseEntity<>(phoneList, HttpStatus.OK);
            }

        } catch (Exception ex) {
            Log.error("PhoneController.readAll: activationId={} from={} to={} phoneTypeAxaptaId={} isBuy={}\n",
                    activationId, fromTimeStamp, toTimeStamp, phoneTypeAxaptaId, isBuy, ex);
            throw ex;
        }
    }

    // Ванина версия запроса
//    @Secured({Roles.AXAPTA/*, Roles.EXT_SYSTEM*/})
//    @GetMapping(name = "readAll", path = "readAll")
//    public ResponseEntity<?> readAll(
//            @RequestParam(name = "activationId", required = false) String activationId,
//            @RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,
//            @RequestParam(name = "from") Timestamp fromTimeStamp,
//            @RequestParam(name = "to") Timestamp toTimeStamp,
//            @RequestParam(name = "phoneTypeAxaptaId", required = false) AXAPTAID phoneTypeAxaptaId,
//            @RequestParam(name = "active", defaultValue = "true") Boolean active,
//            @RequestParam(name = "isBuy", required = false) Boolean isBuy) throws Exception {
//        Log.info("PhoneController.readAll: activationId={} axaptaId={} from={} to={} phoneTypeAxaptaId={} isBuy={}",
//                activationId, (null == axaptaId) ? "null" : axaptaId, fromTimeStamp, toTimeStamp, phoneTypeAxaptaId, isBuy);
//
//        try {
//
//            ProjectAX projectAX = null;
//            // проверка входных параметров
//            if (null != axaptaId && axaptaId.isValid()) {
//                // задан AxaptaId
//                projectAX = projectAXRepository.findByAxaptaId(axaptaId);
//            } else if (null != activationId) {
//                // задан activationId
//                projectAX = projectAXRepository.findByActivationIdAndActive(activationId, true);
//            } else {
//                Log.warn("PhoneController.readAll: Undefined project activationId={} axaptaId={} from={} to={} phoneTypeAxaptaId={} isBuy={}",
//                        activationId, (null == axaptaId) ? "null" : axaptaId, fromTimeStamp, toTimeStamp, phoneTypeAxaptaId, isBuy);
//                return new ResponseEntity<>("PhoneController.readAll: Undefined project id. Set parameter activationId or axaptaId", HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
//            }
//
//            // проверяем валидность UUID проекта
//            if (null == projectAX)
//                throw new IllegalArgumentException("PhoneController.readAll: active Project isn't founded. activationId=" + activationId
//                        + " phoneType=" + phoneTypeAxaptaId);
//
//            if (null != phoneTypeAxaptaId && phoneTypeAxaptaId.isValid()) {
//                // указан тип телефона для выборки
//                PhoneType phoneType = phoneTypeRepository.findByAxaptaId(phoneTypeAxaptaId);
//                Iterable<Phone> phoneList;
//                if (null != phoneType) {
//                    if (null == isBuy) {
//                        phoneList = phoneRepository.findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndPhoneType(
//                                projectAX, fromTimeStamp, toTimeStamp, active, phoneType);
//                    } else if (true == isBuy) {
//                        phoneList = phoneRepository.
//                                findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneNotNullAndPhoneType(
//                                        projectAX, fromTimeStamp, toTimeStamp, active, phoneType);
//
//                    } else {
//                        phoneList = phoneRepository.
//                                findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneIsNullAndPhoneType(
//                                        projectAX, fromTimeStamp, toTimeStamp, active, phoneType);
//
//                    }
//
//                    return new ResponseEntity<>(phoneList, HttpStatus.OK);
//                }
//                throw new IllegalArgumentException("PhoneController.readAll:Unknown PhoneType"
//                        + " activationId=" + activationId
//                        + " from=" + fromTimeStamp
//                        + " to=" + toTimeStamp
//                        + " phoneTypeAxaptaId=" + phoneType
//                        + " isBuy=" + isBuy);
//            } else {
//                Iterable<Phone> phoneList;
//                // не указан тип телефона для выборки
//                if (null == isBuy) {
//                    phoneList = phoneRepository.findAllByProjectAxaptaAndCreateTimeBetweenAndActive(
//                            projectAX, fromTimeStamp, toTimeStamp, active);
//                } else if (true == isBuy) {
//                    phoneList = phoneRepository.
//                            findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneNotNull(
//                                    projectAX, fromTimeStamp, toTimeStamp, active);
//
//                } else {
//                    phoneList = phoneRepository.
//                            findAllByProjectAxaptaAndCreateTimeBetweenAndActiveAndBoughtPhoneIsNull(
//                                    projectAX, fromTimeStamp, toTimeStamp, active);
//
//                }
//
//                return new ResponseEntity<>(phoneList, HttpStatus.OK);
//            }
//
//        } catch (Exception ex) {
//            Log.error("PhoneController.readAll: activationId={} from={} to={} phoneTypeAxaptaId={} isBuy={}\n",
//                    activationId, fromTimeStamp, toTimeStamp, phoneTypeAxaptaId, isBuy, ex);
//            throw ex;
//        }
//    }
//
}