package com.indev.tradein.controller;

import com.indev.security.Roles;
import com.indev.storage.StorageService;
import com.indev.tradein.entity.*;
import com.indev.tradein.repository.ProjectAXRepository;
import com.indev.tradein.repository.TabletPCRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * REST контроллер ресурса планшет TabletPC
 */
@Controller
@RequestMapping("tabletpc")
@Deprecated
public class TabletPCController {

    final TabletPCRepository tabletPCRepository;

    final ProjectAXRepository projectAXRepository;

    private static final Logger Log = LoggerFactory.getLogger(TabletPCController.class);

    private final StorageService storageService;

    @Autowired
    public TabletPCController(TabletPCRepository tabletPCRepository,
                              ProjectAXRepository projectAXRepository,
                              StorageService storageService) {
        this.tabletPCRepository = tabletPCRepository;
        this.projectAXRepository = projectAXRepository;
        this.storageService = storageService;
    }

    //<editor-fold desc="EXCEPTION HANDLER RINE">
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleException(RuntimeException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleException(IllegalArgumentException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleException(NullPointerException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleException(AccessDeniedException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }
    //</editor-fold>

    // Создать планшет
    @Secured({Roles.AXAPTA})
    @PostMapping(name = "create", path = "create")
    public ResponseEntity<?> create(@RequestBody TabletPCPOJO tabletPCPOJO, @RequestParam(name = "axaptaId") AXAPTAID axaptaId) {
        Log.info("TabletPCController.create tabletPC={}  axaptaid={}", tabletPCPOJO, axaptaId);

        try {
            // проверка входных параметров
            TabletPC tablet = tabletPCRepository.findByAxaptaId(axaptaId);
            if (null != tablet) {
                // объект найден, но его не должно быть
                Log.error("TabletPCController.create object already exists {}", tabletPCPOJO);
                // идентификатор запроса не задан или задан с ошибкой, или произошло исключение
                return new ResponseEntity<>(tabletPCPOJO, HttpStatus.CONFLICT);
            }

            // объект не найден, создаем новый
            TabletPC tabletPC = new TabletPC(tabletPCPOJO);

            // если код активации не задан извне, то генерируем UUID автоматически и возвращаем его в структуре.
            if (null == tabletPC.getActivationId())
                tabletPC.setActivationId(UUID.randomUUID().toString());

            // подгружаем структуру проекта
            ProjectAX prj = tabletPC.getProjectAxaptaId();
            if (null != prj) {
                // проект задан, подгружаем его структуру
                if (0 != prj.getId())
                    prj = projectAXRepository.findOne(prj.getId());
                else if (null == prj.getAxaptaId() || prj.getAxaptaId().isValid())
                    prj = projectAXRepository.findByAxaptaId(prj.getAxaptaId());

                if (null == prj)
                    throw new IllegalArgumentException("TabletPCController.create: project not found tabletPC={}"
                            + tabletPC);

                tabletPC.setProjectAxaptaId(prj);
            }

            // принудительно сбрасываем идентификатор БД на невалидный
            tabletPC.setId(0);
            tabletPC.setAxaptaId(axaptaId);
            return new ResponseEntity<>(tabletPCRepository.save(tabletPC), HttpStatus.CREATED);
        } catch (Exception ex) {
            Log.error("TabletPCController.create tabletpc={} axaptaId={}\n", tabletPCPOJO, axaptaId, ex);
            throw ex;
        }

    }

    // Модифицировать планшет
    @Secured({Roles.AXAPTA})
    @PostMapping(name = "update", path = "update")
    public ResponseEntity<?> update(@RequestBody TabletPCPOJO tabletPCPOJO, @RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId) {
        Log.info("TabletPCController.update tabletPC={}  axaptaid={}", tabletPCPOJO, axaptaId);

        try {
            // проверка входных параметров
            TabletPC tabletPC = new TabletPC(tabletPCPOJO);
            tabletPC.setAxaptaId(axaptaId);
            TabletPC tpc = null;
            if (tabletPC.getId() != 0)
                tpc = tabletPCRepository.findOne(tabletPC.getId());
            else if (null != axaptaId)
                tpc = tabletPCRepository.findByAxaptaId(axaptaId);

            if (null != tpc) {
                tpc = UpdateTabletPC(tpc, tabletPC);
                if (null != axaptaId) tpc.setAxaptaId(axaptaId);
                return new ResponseEntity<>(tpc, HttpStatus.OK);
            }
            return new ResponseEntity<>(new TabletPC(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
        } catch (Exception ex) {
            Log.error("TabletPCController.update {} {}\n", tabletPCPOJO, axaptaId, ex);
            throw ex;
        }
    }

    private TabletPC UpdateTabletPC(TabletPC srcTabletPC, TabletPC dstTabletPC) {
        if (null == srcTabletPC)
            throw new IllegalArgumentException("TabletPCController.UpdateTabletPC srcTabletPC can't be null");
        if (null == dstTabletPC)
            throw new IllegalArgumentException("TabletPCController.UpdateTabletPC dstTabletPC can't be null");

        if (null != dstTabletPC.getProjectAxaptaId()) {
            if (0 != dstTabletPC.getProjectAxaptaId().getId()) {
                // задан id для идентификации проекта
                ProjectAX prj = projectAXRepository.findOne(dstTabletPC.getProjectAxaptaId().getId());
                if (null != prj)
                    dstTabletPC.setProjectAxaptaId(prj);
                else
                    throw new IllegalArgumentException("TabletPCController.UpdateTabletPC: Invalid project id="
                            + dstTabletPC.getProjectAxaptaId().getId());
            } else if (null != dstTabletPC.getProjectAxaptaId().getAxaptaId()
                    && dstTabletPC.getProjectAxaptaId().getAxaptaId().isValid()) {
                // задан AxaptaID для идентификации проекта
                ProjectAX prj = projectAXRepository.findByAxaptaId(dstTabletPC.getProjectAxaptaId().getAxaptaId());
                if (null != prj)
                    dstTabletPC.setProjectAxaptaId(prj);
                else
                    throw new IllegalArgumentException("TabletPCController.UpdateTabletPC: Invalid project axaptaId="
                            + dstTabletPC.getProjectAxaptaId().getAxaptaId());

            } else {
                throw new IllegalArgumentException("TabletPCController.UpdateTabletPC: Undefined project id. "
                        + dstTabletPC);
            }


        }

        dstTabletPC.setId(srcTabletPC.getId());
        dstTabletPC.setAxaptaId(srcTabletPC.getAxaptaId());
        dstTabletPC.setCreateTime(srcTabletPC.getCreateTime());

        if (null == dstTabletPC.getImei())
            dstTabletPC.setImei(srcTabletPC.getImei());

        if (null == dstTabletPC.getMac())
            dstTabletPC.setMac(srcTabletPC.getMac());

        // сохранить в БД и возвращаем из базы данных вычитанное новое значение данных по проекту с указанным id
        return tabletPCRepository.save(dstTabletPC);

    }

    /**
     * Прочитать проект. Допустимо указать один из параметров.
     *
     * @param axaptaId идентификатор axapta, должен соответствовать требованиям.
     * @param id       идентификатор по базе.
     * @return Если данные по идентификатору обнаружены, то возвращается OK(200, "OK") и json с данными.
     * Если данные по идентификатору не обнаружены, то возвращаем
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") и пустую json структуру ProjectAX.
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     */
    // #TRAD76 Прочитать планшет
    @Secured({Roles.AXAPTA, Roles.TABLET_PC})
    @GetMapping(name = "read", path = "read")
    public ResponseEntity<?> read
    (@RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,
     @RequestParam(name = "id", required = false, defaultValue = "0") Long id,
     @RequestParam(name = "activationId", required = false) String uuid,
     @RequestParam(name = "imei", required = false) String imei,
     @RequestParam(name = "mac", required = false) String mac,
     @RequestParam(name = "command", defaultValue = "") String command) throws IOException {
        try {
            Log.info("TabletPCController.read: id={} axaptaid={} activationId={} command={} imei=[] mac={}",
                    id, axaptaId, uuid, command, imei, mac);

            TabletPC tabPC = null;
            // проверка входных параметров
            if (id != 0) {
                // задан Id базы данных
                tabPC = tabletPCRepository.findOne(id);
            } else if (null != axaptaId) {
                // задан AxaptaId
                tabPC = tabletPCRepository.findByAxaptaId(axaptaId);
            } else if (uuid != null) {
                // задан uuid
                tabPC = tabletPCRepository.findByActivationId(uuid);
            }
            // проверяем что мы счтали из базы информацию о планшете
            if (null == tabPC)
                return new ResponseEntity<>(new TabletPC(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

            switch (command) {
                case "getlogo":
                    return onCommandGetLogo(tabPC);
                case "":
                    // команда не задана
                    boolean reqUpdate = false;
                    if (null != mac && !mac.equals(tabPC.getMac())) {
                        tabPC.setMac(mac);
                        reqUpdate = true;
                    }

                    if (null != imei && !imei.equals(tabPC.getImei())) {
                        tabPC.setImei(imei);
                        reqUpdate = true;
                    }
                    // обновляем в БД mac и imei
                    if (reqUpdate == true) {
                        Log.warn("TabletPCController.read: store new IMEI or MAC id={} axaptaid={} activationId={} command={} imei=[] mac={}",
                                id, axaptaId, uuid, command, imei, mac);
                        tabPC = tabletPCRepository.save(tabPC);
                    }

                    break;
                // какая-то другая неверная команда
                default: {
                    Log.error("TabletPCController.read id={} axaptaid={} command={}. Unknown command", id, axaptaId, command);
                    throw new IllegalArgumentException("TabletPCController.read Unknown command=" + command);
                }
            }

            return new ResponseEntity<>(tabPC, HttpStatus.OK);

        } catch (Exception ex) {
            Log.error("TabletPCController.read: axaptaid={} id={} command={}\n", axaptaId, id, command, ex);
            throw ex;
        }
    }

    private ResponseEntity<?> onCommandGetLogo(TabletPC tabPC) throws IOException {

        ProjectAX prj = tabPC.getProjectAxaptaId();

        if (null != prj && prj.getId() != 0) {
            String logoFileName = tabPC.getProjectAxaptaId().getLogoFile();
            if (null != logoFileName) {

                // корректировка пути хранилища проекта
                // сохранение файлов
                if (null == logoFileName)
                    return new ResponseEntity<>(prj, HttpStatus.NO_CONTENT);

                String relativeFileName = "project/" + prj.getId() + "/" + logoFileName;

                Resource file = storageService.serveItemAsResource(relativeFileName);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.IMAGE_PNG);
                return new ResponseEntity<>(file, headers, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(tabPC, HttpStatus.BAD_REQUEST);
    }
}