package com.indev.tradein.controller;

import com.indev.storage.StorageProperties;
import com.indev.tradein.ProcessProperties;
import com.indev.tradein.entity.About;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST контроллер ресурса About
 */
@RestController
public class AboutController {

    private static final Logger Log = LoggerFactory.getLogger(AboutController.class);

    private StorageProperties storageProperties;
    private ProcessProperties processProperties;

    @Autowired
    AboutController(StorageProperties storageProperties, ProcessProperties processProperties)
    {
        this.storageProperties = storageProperties;
        this.processProperties = processProperties;
        
    }

    //<editor-fold desc="EXCEPTION HANDLER RINE">
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleException(RuntimeException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleException(IllegalArgumentException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleException(NullPointerException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleException(AccessDeniedException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }
    //</editor-fold>

//  @Secured({Roles.AXAPTA, Roles.TABLET_PC, Roles.EXT_SYSTEM})
    @RequestMapping("/about")
    public About about()
    {
        Log.info("AboutController.about");
        return new About(storageProperties.getLocation(),
                processProperties.getApiVersion(),
                processProperties.getApkVersion());
    }
}
