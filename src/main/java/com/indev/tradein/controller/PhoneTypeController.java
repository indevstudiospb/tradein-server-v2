package com.indev.tradein.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.indev.security.Roles;
import com.indev.storage.StorageService;
import com.indev.tradein.entity.*;
import com.indev.tradein.repository.PhoneTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * REST контроллер ресурса типы диагностиремых телефонов PhoneType
 */
@RestController
@RequestMapping("phonetype")
public class PhoneTypeController {

    private final PhoneTypeRepository phoneTypeRepository;
    private final StorageService storageService;

    private static final Logger Log = LoggerFactory.getLogger(PhoneTypeController.class);

    @Autowired
    PhoneTypeController(PhoneTypeRepository phoneTypeRepository, StorageService storageService) {
        this.phoneTypeRepository = phoneTypeRepository;
        this.storageService = storageService;
    }

    //<editor-fold desc="EXCEPTION HANDLER RINE">
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleException(RuntimeException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleException(IllegalArgumentException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleException(NullPointerException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleException(AccessDeniedException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }
    //</editor-fold>

    // TODO скорректировать описание

    /**
     * Создать запись проекта в БД.
     * <p>
     * POST метод.
     * <p>
     * В качестве входных данных принимает JSON структуру ProjectAX.
     * Внимание! Поле axaptaid должно быть заполнено и содержать валидное значение и ранее не использоваться
     * <p>
     * Структура хранения данных в файловом хранилище https://indevpro.atlassian.net/wiki/spaces/TRAD/pages/13729793
     *
     * @param phoneType - десерлизованный класс ProjectAX, который необходимо создать в БД.
     * @return В случае успешного выполнения, метод возвращает CREATED(201, "Created") и json с созданным проектом.
     * <p>
     * Если проект по идентификатору axaptaid уже были ранее создан, то метод возвращает CONFLICT(409, "Conflict") и
     * исходную структуру ProjectAX.
     * <p>
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * <p>
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     * @<code> curl -isb -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"id":1,"axaptaId":"0000000003","logoFile":null,"active":true,"createTime":1504008707422,"updateTime":1504027006611,"jsonPhoneDiagnosticSchema":"test"}' http://localhost:8080/project/create</code>
     */
    // #TRAD-80 DONE создать
    @Secured({Roles.AXAPTA})
    @PostMapping(name = "create", path = "create")
    public ResponseEntity<?> create(@RequestBody PhoneType phoneType,
                                    @RequestParam(name = "axaptaId") AXAPTAID axaptaId) {
        Log.info("PhoneTypeController.create {}", phoneType);

        HttpStatus respondCode = HttpStatus.BAD_REQUEST;
        Object errorBodyRespond = new PhoneType();

        try {
            // проверка входных параметров
            if (null != phoneType) {
                if (axaptaId.isValid()) {
                    errorBodyRespond = phoneTypeRepository.findByAxaptaId(axaptaId);
                    if (null == errorBodyRespond) {
                        // объект не найден, создаем новый
                        // принудительно сбрасываем идентификатор БД на невалидный
                        phoneType.setId(0);
                        phoneType.setAxaptaId(axaptaId);
                        return new ResponseEntity<>(new PhoneTypeEx(phoneTypeRepository.save(phoneType)), HttpStatus.CREATED);
                    } else {
                        // объект найден, но его не должно быть
                        Log.error("PhoneTypeController.create object already exists {}", phoneType);
                        respondCode = HttpStatus.CONFLICT;
                        errorBodyRespond = phoneType;

                    }
                } else {
                    // задан неверный формат axaptaid
                    Log.error("PhoneTypeController.create invalid axaptaid {}", phoneType);
                }
            }

        } catch (Exception ex) {
            Log.error("PhoneTypeController.create ", ex);
            respondCode = HttpStatus.INTERNAL_SERVER_ERROR;
            errorBodyRespond = ex.getMessage();
        }
        // идентификатор запроса не задан или задан с ошибкой, или произошло исключение
        return new ResponseEntity<>(errorBodyRespond, respondCode);
    }


    /**
     * Прочитать проект. Допустимо указать один из параметров.
     *
     * @param axaptaId идентификатор axapta, должен соответствовать требованиям.
     * @param id       идентификатор по базе.
     * @return Если данные по идентификатору обнаружены, то возвращается OK(200, "OK") и json с данными.
     * Если данные по идентификатору не обнаружены, то возвращаем
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") и пустую json структуру ProjectAX.
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     */
    @Secured({Roles.AXAPTA, Roles.TABLET_PC})
    @GetMapping(name = "read", path = "read")
    public ResponseEntity<?> read
    (@RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,
     @RequestParam(name = "id", defaultValue = "0") Long id,
     @RequestParam(name = "command", defaultValue = "") String command) {
        Log.info("PhoneTypeController.read id={} axaptaid={} command={}", id, axaptaId, command);

        HttpStatus respondCode = HttpStatus.BAD_REQUEST;
        Object errorBodyRespond = new PhoneType();
        try {
            PhoneType phType = null;

            // проверка входных параметров
            if (id != 0) {
                // задан Id базы данных
                phType = phoneTypeRepository.findOne(id);
            } else if (null != axaptaId) {
                // задан AxaptaId
                phType = phoneTypeRepository.findByAxaptaId(axaptaId);
            } else
                return new ResponseEntity<>(new PhoneType(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

            respondCode = (phType != null) ? HttpStatus.OK : HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE;
            phType = (phType != null) ? phType : new PhoneType();

            switch (command) {
                case "getschema":
                    return onCommandGetSchema(phType);
                case "":
                    break;
                default: {
                    throw new IllegalArgumentException("Unknown command " + command);
                }
            }

            return new ResponseEntity<>(new PhoneTypeEx(phType), respondCode);

        } catch (Exception ex) {
            Log.error("PhoneTypeController.read id={} axaptaid={} command={}", id, axaptaId, command, ex);
            throw ex;
        }
    }

    private ResponseEntity<?> onCommandGetSchema(PhoneType phoneType) {
        if (null == phoneType)
            throw new NullPointerException("PhoneTypeController.onCommandGetSchema():prj");

        if (null == phoneType.getTestSchema())
            return new ResponseEntity<>("", HttpStatus.NO_CONTENT);

        String schema = phoneType.getTestSchema().getJsonPhoneDiagnosticSchema();

        return ResponseEntity.ok().
                header(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8").body(schema);
    }


    /**
     * Прочитать все активные типы телефонов.
     *
     * @return JSON Перечень активных типов телефонов и OK(200, "OK").
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     * <code>curl -i http://192.168.1.75:8080/phonetype/readall</code>
     */
    @Deprecated
    @Secured({Roles.AXAPTA, Roles.TABLET_PC})
    @GetMapping(name = "readAll", path = "readAll")
    public ResponseEntity<?> readAll(@RequestParam(name = "active", defaultValue = "true") Boolean active) {
        HttpStatus respondCode = HttpStatus.BAD_REQUEST;
        Object errorBodyRespond;
        try {
            Log.info("PhoneTypeController.readAll active={}", active);
            // проверка входных параметров
            Iterable<PhoneType> phTypeList = phoneTypeRepository.findByActive(active);
            return new ResponseEntity<>(phTypeList, HttpStatus.OK);
        } catch (Exception ex) {
            Log.error("PhoneTypeController.readAll Can't read phoneTypeRepository.findByActive", ex);
            throw new RuntimeException("Can't read phoneTypeRepository.findByActive", ex);
        }
    }


    /**
     * Модифицировать запись проекта в БД.
     * POST метод. В качестве входных данных принимает JSON структуру ProjectAX.
     * <p>
     * URL метода может принимать параметр ?command=имя_команды
     * <p>
     * Структура хранения данных в файловом хранилище https://indevpro.atlassian.net/wiki/spaces/TRAD/pages/13729793
     *
     * @param stringPhoneType - серлизованный класс ProjectAX, который необходимо записать в БД.
     * @return Если данные по идентификатору обнаружены, то возвращается OK(200, "OK") и json с обновленной структурой данных.
     * Если данные по идентификатору не обнаружены, то возвращаем
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") и пустую json структуру ProjectAX.
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     * @<code> curl -isb -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"id":1,"axaptaId":"0000000001","logoFile":null,"active":true,"createTime":1504008707422,"updateTime":1504027006611,"jsonPhoneDiagnosticSchema":null}' http://localhost:8080/project/update
     * </code>
     */
    // #TRAD-80 модифицировать информацию по проекту
    // https://indevpro.atlassian.net/wiki/spaces/TRAD/pages/13729793
    @Secured({Roles.AXAPTA})
    @PostMapping(name = "update", path = "update")
    public ResponseEntity<?> update(
            @RequestParam(name = "phoneType") String stringPhoneType,
            @RequestParam(name = "axaptaId") AXAPTAID axaptaId,
            @RequestParam(name = "schema", required = false) String jsonSchema) {

        Log.info("PhoneTypeController.update {} {}\n{}", stringPhoneType, axaptaId, jsonSchema);

        PhoneType phoneType;

        // проверка входных параметров
        if (null == stringPhoneType)
            throw new NullPointerException("PhoneTypeController.update:parameter stringPhoneType must be defined.");

        // десерилизуем объект из строки
        try {
            ObjectMapper mapper = new ObjectMapper();
            phoneType = mapper.readValue(stringPhoneType, PhoneType.class);
        } catch (IOException ex) {
            Log.warn("PhoneTypeController.update: Can't create PhoneType from string {}", stringPhoneType);
            throw new IllegalArgumentException("PhoneTypeController.update: Can't create PhoneType from string " + stringPhoneType, ex);
        }

        HttpStatus respondCode;
        try {
            PhoneType pht = null;
            if (null != phoneType)
                if (phoneType.getId() != 0) {
                    // задан Id базы данных
                    pht = phoneTypeRepository.findOne(phoneType.getId());
                } else if (null != axaptaId && axaptaId.isValid() ) {
                    // задан AxaptaId
                    pht = phoneTypeRepository.findByAxaptaId(axaptaId);
                } else
                    return new ResponseEntity<>(new PhoneTypeEx(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

            respondCode = (pht != null) ? HttpStatus.OK : HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE;

            if (null != jsonSchema)
                phoneType.setTestSchema(new TestSchema(jsonSchema));

            // установка схемы диагностики
            if (null != jsonSchema) {
                // каждый раз при обновлении создается новая схема, если ранее схемы не было или она отличалась
                Log.info("PhoneTypeController.update: preparing change schema to {}", jsonSchema);
                if (jsonSchema.isEmpty())
                    phoneType.setTestSchema(null);      // команда на сброс схемы проекта
                else if (null != pht.getTestSchema()) {
                    if (!pht.getTestSchema().getJsonPhoneDiagnosticSchema().equals(jsonSchema))
                        phoneType.setTestSchema(new TestSchema(jsonSchema));    // схема изменилась создаем новую
                    else
                        phoneType.setTestSchema(pht.getTestSchema());   // схема не изменилась
                } else
                    phoneType.setTestSchema(new TestSchema(jsonSchema)); // ранее схемы не было теперь создаем
            } else
                phoneType.setTestSchema(pht.getTestSchema());

            pht = (pht != null) ? UpdatePhoneType(pht, phoneType) : new PhoneType();

            return new ResponseEntity<>(new PhoneTypeEx(pht), respondCode);

        } catch (Exception ex) {
            Log.error("PhoneTypeController.update {}\n", phoneType, ex);
            throw ex;
        }
    }

    private PhoneType UpdatePhoneType(PhoneType srcPhoneType, PhoneType dstPhoneType) {
        if (null == srcPhoneType)
            throw new IllegalArgumentException("srcPhoneType can't be null");
        if (null == dstPhoneType)
            throw new IllegalArgumentException("dstPhoneType can't be null");

        dstPhoneType.setId(srcPhoneType.getId());
        dstPhoneType.setAxaptaId(srcPhoneType.getAxaptaId());
        dstPhoneType.setCreateTime(srcPhoneType.getCreateTime());
        // сохранить в БД и возвращаем из базы данных вычитанное новое значение данных по проекту с указанным id
        return phoneTypeRepository.save(dstPhoneType);

    }

}

