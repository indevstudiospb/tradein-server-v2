package com.indev.tradein.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.indev.security.Roles;
import com.indev.storage.StorageService;
import com.indev.tradein.entity.*;
import com.indev.tradein.repository.PhoneTypeRepository;
import com.indev.tradein.repository.ProjectAXRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

/**
 * REST контроллер ресурса ProjectAX
 */
@RestController
@RequestMapping("project")
public class ProjectAXController {


    private static final Logger Log = LoggerFactory.getLogger(ProjectAXController.class);

    private final ProjectAXRepository projectAXRepository;

    private final PhoneTypeRepository phoneTypeRepository;

    private final StorageService storageService;

    @Autowired
    ProjectAXController(ProjectAXRepository projectAXRepository, PhoneTypeRepository phoneTypeRepository, StorageService storageService) {
        this.projectAXRepository = projectAXRepository;
        this.phoneTypeRepository = phoneTypeRepository;
        this.storageService = storageService;
    }

    //<editor-fold desc="EXCEPTION HANDLER RINE">
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleException(RuntimeException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleException(IllegalArgumentException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<?> handleException(NullPointerException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleException(AccessDeniedException ex) {
        Log.error(ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
    }
    //</editor-fold>

    /**
     * Загрузить файл логотипа на сервер
     *
     * @param axaptaId - идентификатор проекта по Axapta
     * @param id       - идентификатор проекта по БД
     * @param file     - файл логотипа
     * @return ResponseEntity
     * 200(OK) - если запрос успешно отработал
     * 416(REQUESTED_RANGE_NOT_SATISFIABLE) - если не найден проект с указанным идентификатором
     * @
     */
    @Secured({Roles.AXAPTA, Roles.TABLET_PC})
    @PostMapping(path = "setLogo", name = "setLogo")
    public ResponseEntity<?> setLogo(@RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,
                                     @RequestParam(name = "id", required = false, defaultValue = "0") Long id,
                                     @RequestParam(name = "file", required = false) MultipartFile file) {
        Log.info("ProjectAX.setLogo id={} axaptaId={}", id, axaptaId);

        ProjectAX prj;
        // проверка входных параметров
        if (id != 0) {
            // задан Id базы данных
            prj = projectAXRepository.findOne(id);
        } else if (null != axaptaId && axaptaId.isValid()) {
            // задан AxaptaId
            prj = projectAXRepository.findByAxaptaId(axaptaId);
        } else
            return new ResponseEntity<>(new ProjectAX(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

        // проверяем что мы счтали из базы информацию о проекте
        if (null == prj)
            return new ResponseEntity<>(new ProjectAX(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

        if (null != file) {
            // корректировка пути хранилища проекта
            // сохранение файлов
            String relativePath = "project/" + prj.getId() + "/";

            storageService.storeItem(file, relativePath, true);

            // обновляем проект, сохраняем имя логотипа
            prj.setLogoFile(file.getOriginalFilename());

        } else {
            // сбросить логотип в null
            prj.setLogoFile(null);
        }

        prj = projectAXRepository.save(prj);

        return new ResponseEntity<>(prj, HttpStatus.OK);
    }

    /**
     * Выгрузить файл логотипа с сервера
     *
     * @param axaptaId - идентификатор проекта по Axapta
     * @param id       - идентификатор проекта по БД
     * @param logoName - имя файла для выгрузки
     * @return ResponseEntity
     * OK(200, "OK") - если запрос успешно отработал
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") - если не найден проект с указанным идентификатором
     * NO_CONTENT(204, "No Content")
     */
    @Secured({Roles.AXAPTA, Roles.TABLET_PC})
    @GetMapping(path = "getLogo", name = "getLogo")
    public ResponseEntity<?> getLogo(@RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,
                                     @RequestParam(name = "id", required = false, defaultValue = "0") Long id,
                                     @RequestParam("logoName") String logoName) throws IOException {
        Log.info("ProjectAX.getLogo id={} axaptaId={}", id, axaptaId);
        ProjectAX prj = null;
        // проверка входных параметров
        if (id != 0) {
            // задан Id базы данных
            prj = projectAXRepository.findOne(id);
        } else if (null != axaptaId && axaptaId.isValid()) {
            // задан AxaptaId
            prj = projectAXRepository.findByAxaptaId(axaptaId);
        } else
            return new ResponseEntity<>(new ProjectAX(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

        // проверяем что мы счтали из базы информацию о проекте
        if (null == prj)
            return new ResponseEntity<>(new ProjectAX(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

        // корректировка пути хранилища проекта
        // сохранение файлов
        if (null == prj.getLogoFile())
            return new ResponseEntity<>(prj, HttpStatus.NO_CONTENT);

        String relativeFileName = "project/" + prj.getId() + "/" + logoName;

        Resource file = storageService.serveItemAsResource(relativeFileName);


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<>(file, headers, HttpStatus.OK);
    }


    /**
     * Создать запись проекта в БД.
     * <p>
     * POST метод.
     * <p>
     * В качестве входных данных принимает JSON структуру ProjectAX.
     * Внимание! Поле axaptaid должно быть заполнено и содержать валидное значение и ранее не использоваться
     * <p>
     * Структура хранения данных в файловом хранилище https://indevpro.atlassian.net/wiki/spaces/TRAD/pages/13729793
     *
     * @param projectAX - десерлизованный класс ProjectAX, который необходимо создать в БД.
     * @return В случае успешного выполнения, метод возвращает CREATED(201, "Created") и json с созданным проектом.
     * <p>
     * Если проект по идентификатору axaptaid уже были ранее создан, то метод возвращает CONFLICT(409, "Conflict") и
     * исходную структуру ProjectAX.
     * <p>
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * <p>
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     * @<code> curl -isb -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d '{"id":1,"axaptaId":"0000000003","logoFile":null,"active":true,"createTime":1504008707422,"updateTime":1504027006611,"jsonPhoneDiagnosticSchema":"test"}' http://localhost:8080/project/create</code>
     */
    @Secured({Roles.AXAPTA})
    @PostMapping(name = "create", path = "create")
    public ResponseEntity<?> create(@RequestBody ProjectAX projectAX,
                                    @RequestParam(name = "axaptaId", required = true) AXAPTAID axaptaId) {

        Log.info("ProjectAX.create {} axaptaid={}", projectAX, axaptaId);

        HttpStatus respondCode = HttpStatus.BAD_REQUEST;
        Object errorBodyRespond = new ProjectAX();

        try {
            // проверка входных параметров
            if (null != projectAX) {
                if (null != axaptaId) {
                    errorBodyRespond = projectAXRepository.findByAxaptaId(projectAX.getAxaptaId());
                    if (null == errorBodyRespond) {
                        // объект не найден, создаем новый
                        // принудительно сбрасываем идентификатор БД на невалидный
                        projectAX.setId(0);
                        projectAX.setAxaptaId(axaptaId);

                        // если код активации не задан, то присваиваем автоматически
                        if (null == projectAX.getActivationId())
                            projectAX.setActivationId(UUID.randomUUID().toString());

                        return new ResponseEntity<>(projectAXRepository.save(projectAX), HttpStatus.CREATED);
                    } else {
                        // объект найден, но его не должно быть
                        Log.error("ProjectAX.create object already exists {}", projectAX);
                        respondCode = HttpStatus.CONFLICT;
                        errorBodyRespond = projectAX;

                    }
                } else {
                    Log.error("ProjectAX.create invalid axaptaid {}", projectAX);
                }
            }

        } catch (Exception ex) {
            Log.error("ProjectAX.create ", ex);
            respondCode = HttpStatus.INTERNAL_SERVER_ERROR;
            errorBodyRespond = ex.getMessage();
        }
        // идентификатор запроса не задан или задан с ошибкой, или произошло исключение
        return new ResponseEntity<>(errorBodyRespond, respondCode);
    }


    /**
     * Прочитать проект. Допустимо указать один из параметров.
     *
     * @param axaptaId идентификатор axapta, должен соответствовать требованиям.
     * @param id       идентификатор по базе.
     * @return Если данные по идентификатору обнаружены, то возвращается OK(200, "OK") и json с данными.
     * Если данные по идентификатору не обнаружены, то возвращаем
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") и пустую json структуру ProjectAX.
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     */
    @Secured({Roles.AXAPTA, Roles.TABLET_PC, Roles.EXT_SYSTEM})
    @GetMapping(name = "read", path = "read", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> read
    (@RequestParam(name = "axaptaId", required = false) AXAPTAID axaptaId,
     @RequestParam(name = "id", required = false, defaultValue = "0") Long id,
     @RequestParam(name = "activationId", required = false) String activationId,
     @RequestParam(name = "command", defaultValue = "") String command) {
        try {
            Log.info("ProjectAX.read id={} axaptaid={} command={}", id, axaptaId, command);

            ProjectAX prj = null;
            // проверка входных параметров
            if (id != 0) {
                // задан Id базы данных
                prj = projectAXRepository.findOne(id);
            } else if (null != axaptaId) {
                // задан AxaptaId
                prj = projectAXRepository.findByAxaptaId(axaptaId);
            } else if (null != activationId) {
                // задан activationId
                prj = projectAXRepository.findByActivationId(activationId);
            } else
                return new ResponseEntity<>(new ProjectAX(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

            // проверяем что мы счтали из базы информацию о проекте
            if (null == prj)
                return new ResponseEntity<>(new ProjectAX(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

            switch (command) {
                case "getstorage":
                    return onCommandGetStorage(prj);
                case "getschema":
                    return onCommandGetSchema(prj);
                case "getresource":
                    return onCommandGetResource(prj);
                case "":
                    break;
                default: {
                    Log.error("ProjectAX.read id={} axaptaid={} command={}. Unknown command", id, axaptaId, command);
                    throw new IllegalArgumentException("Unknown command " + command);
                }
            }

            return new ResponseEntity<>(prj, HttpStatus.OK);

        } catch (Exception ex) {
            Log.error("read: axaptaid={} id={} command={} ", axaptaId, id, command, ex);
            throw ex;
        }
    }

    /**
     * Прочитать список проектов.
     * Если проекты не обнаружены, то возвращаем  NO_CONTENT
     */
    @Secured({Roles.AXAPTA})
    @GetMapping(name = "shortlist", path = "shortlist", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> readAllShort() {

        try {
            Log.info("ProjectAX.readAllShort()");

            ArrayList<ProjectAxShortDto> result = new ArrayList<>();
            Iterable<ProjectAX> list = projectAXRepository.findAllOrderById();

            if (null == list)
                return ResponseEntity.noContent().build();

            list.forEach(prj -> result.add(
                    ProjectAxShortDto.builder()
                            .axaptaId(prj.getAxaptaId().getAxaptaId())
                            .caption(prj.getCaption())
                            .projectId(prj.getId())
                            .active(prj.getActive())
                            .activationId(prj.getActivationId())
                            .build())
            );

            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (Exception ex) {
            Log.error("readAllShort(): ", ex);
            throw ex;
        }
    }

    private ResponseEntity<?> onCommandGetResource(ProjectAX prj) {
        if (null == prj)
            throw new NullPointerException("ProjectAXController.onCommandGetSchema():prj");

        if (null == prj.getResource())
            return new ResponseEntity<>("", HttpStatus.NO_CONTENT);

        String resource = prj.getResource().getValue();

        return ResponseEntity.ok().
                header(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8").body(resource);
    }

    private ResponseEntity<?> onCommandGetSchema(ProjectAX prj) {
        if (null == prj)
            throw new NullPointerException("ProjectAXController.onCommandGetSchema():prj");

        if (null == prj.getTestSchema())
            return new ResponseEntity<>("", HttpStatus.NO_CONTENT);

        String schema = prj.getTestSchema().getJsonPhoneDiagnosticSchema();

        return ResponseEntity.ok().
                header(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8").body(schema);
    }

    private ResponseEntity<?> onCommandGetStorage(ProjectAX prj) {
        if (null == prj)
            throw new NullPointerException("ProjectAXController.onCommandGetStorage prj");

        return new ResponseEntity<>(new ProjectAXStorage(prj), HttpStatus.OK);
    }

    /**
     * Модифицировать запись проекта в БД.
     * POST метод. В качестве входных данных принимает JSON структуру ProjectAX.
     * <p>
     * URL метода может принимать параметр ?command=имя_команды
     * <p>
     * Структура хранения данных в файловом хранилище https://indevpro.atlassian.net/wiki/spaces/TRAD/pages/13729793
     *
     * @param stringProjectAX - серлизованный класс ProjectAX, который необходимо записать в БД.
     * @return Если данные по идентификатору обнаружены, то возвращается OK(200, "OK") и json с обновленной структурой данных.
     * Если данные по идентификатору не обнаружены, то возвращаем
     * REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested range not satisfiable") и пустую json структуру ProjectAX.
     * Если идентификаторы не заданы, то возвращаем BAD_REQUEST(400, "Bad Request") и пустую json структуру ProjectAX.
     * Если произошло исключение, то возвращаем INTERNAL_SERVER_ERROR(500, "Internal Server Error") и текст исключения.
     */
    @Secured({Roles.AXAPTA})
    @PostMapping(name = "update", path = "update")
    public ResponseEntity<?> update(
            @RequestParam(name = "project") String stringProjectAX,
            @RequestParam(name = "typeId", required = false) Set<AXAPTAID> typeIds,
            @RequestParam(name = "axaptaId") AXAPTAID axaptaId,
            @RequestParam(name = "storage", required = false) String storage,
            @RequestParam(name = "schema", required = false) String jsonSchema,
            @RequestParam(name = "resource", required = false) String resource) {

        Log.info("ProjectAX.update stringProjectAX={}\naxaptaid={}\nstorage={}\njsonSchema={}\ntypeIds={}\n",
                stringProjectAX, axaptaId, storage, jsonSchema, typeIds);

        HttpStatus respondCode = HttpStatus.BAD_REQUEST;

        try {
            // проверка входных параметров
            if (null == stringProjectAX)
                throw new NullPointerException("update:ProjectAX:parameter project must be defined.");

            ProjectAX projectAX = null;

            // десерилизуем объект из строки
            try {
                ObjectMapper mapper = new ObjectMapper();
                projectAX = mapper.readValue(stringProjectAX, ProjectAX.class);
            } catch (IOException ex) {
                Log.warn("ProjectAX.update: Can't create ProjectAX from string {}", stringProjectAX);
                throw new IllegalArgumentException("Can't create ProjectAX from string " + stringProjectAX, ex);
            }

            ProjectAX prj = null;

            if (projectAX.getId() != 0) {
                // задан Id базы данных
                prj = projectAXRepository.findOne(projectAX.getId());
            } else if (null != axaptaId && axaptaId.isValid()) {
                prj = projectAXRepository.findByAxaptaId(axaptaId);
            }
            if (null != prj) {
                // установка пути хранилищу
                if (null != storage) {
                    Log.info("ProjectAX.update: preparing change storage to {} for project id={} axaptaid={}", storage, prj.getId(), prj.getAxaptaId());
                    if (storage.isEmpty())
                        projectAX.setStoragePath(null);      // команда на сброс пути хранилища проекта
                    else
                        projectAX.setStoragePath(storage);
                }
                // установка схемы диагностики
                if (null != jsonSchema) {
                    // каждый раз при сохранении создается схемы диагностики создается новая схема, если ранее схемы не было или она отличалась
                    Log.info("ProjectAX.update: preparing change schema to {} \nfor project id={} axaptaid={}", jsonSchema, prj.getId(), prj.getAxaptaId());
                    if (jsonSchema.isEmpty())
                        projectAX.setTestSchema(null);      // команда на сброс схемы проекта
                    else if (null != prj.getTestSchema()) {
                        if (!prj.getTestSchema().getJsonPhoneDiagnosticSchema().equals(jsonSchema))
                            projectAX.setTestSchema(new TestSchema(jsonSchema));    // схема изменилась создаем новую
                        else
                            projectAX.setTestSchema(prj.getTestSchema());   // схема не изменилась
                    } else
                        projectAX.setTestSchema(new TestSchema(jsonSchema)); // ранее схемы не было теперь создаем
                } else
                    projectAX.setTestSchema(prj.getTestSchema());

                // установка ресурсов
                if (null != resource) {
                    // каждый раз при сохранении создаются новые ресерусры, если ранее ресурсов не было или они отличаются
                    Log.info("ProjectAX.update: preparing change resource to {} \nfor project id={} axaptaid={}",
                            resource, prj.getId(), prj.getAxaptaId());
                    if (resource.isEmpty())
                        projectAX.setResource(null);      // команда на сброс ресурсов проекта
                    else if (null != prj.getResource()) {
                        if (!prj.getResource().getValue().equals(resource))
                            projectAX.setResource(new ProjectResource(resource));    // схема изменилась создаем новую
                        else
                            projectAX.setResource(prj.getResource());   // схема не изменилась
                    } else {
                        projectAX.setResource(new ProjectResource(resource)); // ранее ресурсов не было теперь создаем
                    }
                } else
                    projectAX.setResource(prj.getResource());

            }

            Set<PhoneType> phoneTypes = null;
            // проверка необходимости корректировки привязки типов к проекту
            if (null != typeIds) {
                phoneTypes = phoneTypeRepository.findAllByAxaptaIdIn(typeIds);
                // корректируем привязку типов телефонов, если параметр была задан
                projectAX.setPhoneTypes(phoneTypes);
            }

            respondCode = (prj != null) ? HttpStatus.OK : HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE;
            prj = (prj != null) ? projectAXRepository.save(UpdateProjectAX(prj, projectAX)) : new ProjectAX();

            return new ResponseEntity<>(prj, respondCode);

        } catch (Exception ex) {
            Log.error("ProjectAX.update ProjectAX={}\n storage={}\n jsonSchema={}\n", stringProjectAX, storage, jsonSchema, ex);
            throw ex;
        }
    }

    private ProjectAX UpdateProjectAX(ProjectAX srcProjectAX, ProjectAX dstProjectAX) {
        if (null == srcProjectAX)
            throw new IllegalArgumentException("srcProjectAX can't be null");
        if (null == dstProjectAX)
            throw new IllegalArgumentException("dstProjectAX can't be null");

        // если не указан activationId, то сохраняем предыдущее значение activationId
        if (null == dstProjectAX.getActivationId())
            dstProjectAX.setActivationId(srcProjectAX.getActivationId());

        // если типы телефонов не заданы то сохраняем предыдущие
        if (null == dstProjectAX.getPhoneTypes())
            dstProjectAX.setPhoneTypes(srcProjectAX.getPhoneTypes());

        dstProjectAX.setId(srcProjectAX.getId());
        dstProjectAX.setAxaptaId(srcProjectAX.getAxaptaId());
        dstProjectAX.setCreateTime(srcProjectAX.getCreateTime());
        return dstProjectAX;

    }

}
