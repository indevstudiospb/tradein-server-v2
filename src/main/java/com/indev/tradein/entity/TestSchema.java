package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "test_schema", schema = "tradein"/*, uniqueConstraints = {@UniqueConstraint(columnNames = {"axapta_id"})}*/)
public class TestSchema {

    public TestSchema() {

    }

    public TestSchema(String jsonSchema) {
        this.jsonPhoneDiagnosticSchema = jsonSchema;
    }


    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator( name = "testSchemaSequence", sequenceName = "TEST_SCHEMA_SEQUENCE", allocationSize = 1, initialValue = 100 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "testSchemaSequence")
    @Column(name = "test_schema_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    //<editor-fold desc="AXAPTA ID">
    // идентификатор по Axapta
    private AXAPTAID axaptaId;

//    @JsonIgnore
//    public AXAPTAID getAxaptaId() {
//        return axaptaId;
//    }
//
//    public void setAxaptaId(AXAPTAID axaptaId) {
//        this.axaptaId = axaptaId;
//    }
//    //</editor-fold>
//
//    //<editor-fold desc="PHONE DIAGNOSTIC SCHEMA">

    // json схема диагностического теста
    private String jsonPhoneDiagnosticSchema;
    /**
     * Схема диагностики смартфонов для данного проекта.
     * Если схема не задана, то возвращается значение null
     * и в этом случае диагностическое приложение проводит набор тестов по умолчанию.
     *
     * @return - схема дигностики смартфонов для данного проекта в формате JSON.
     * Допускается значени null.
     */
    @JsonIgnore
    @Column(name = "phone_diagnostic_schema", length = 32768)
    public String getJsonPhoneDiagnosticSchema() {
        return jsonPhoneDiagnosticSchema;
    }

    public void setJsonPhoneDiagnosticSchema(String jsonPhoneDiagnosticSchema) {
        this.jsonPhoneDiagnosticSchema = jsonPhoneDiagnosticSchema;
    }
    //</editor-fold>

}
