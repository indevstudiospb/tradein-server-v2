package com.indev.tradein.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Краткий диагностический отчет
 */
@ToString
public class DiagnosticReportBrief {

    public DiagnosticReportBrief(final DiagnosticReport diagnosticReport) {
        if( null == diagnosticReport )
            throw new NullPointerException("DiagnosticReportBrief.DiagnosticReportBrief: diagnosticReport");

        this.id = diagnosticReport.getId();
        this.axaptaId = diagnosticReport.getAxaptaId();
        this.active = diagnosticReport.getActive();
    }

    // идентификатор по базе
    @Getter @Setter
    private long id;

    // идентификатор по axapta
    @Getter @Setter
    private String axaptaId;

    // признак активности отчета
    @Getter @Setter
    private Boolean active = true;

}
