package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;


/**
 * Created by ivan on 22.08.17.
 */
@ToString
@JsonIgnoreProperties(ignoreUnknown=true)
@Deprecated
public class TabletPCPOJO {

    @Getter @Setter
    private long id;

    @Getter @Setter
    private AXAPTAID axaptaId;

    @Getter @Setter
    private Boolean active = true;

    @Getter @Setter
    // дата и время создания
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    // дата и время последнего обновления
    @Getter @Setter
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    public Timestamp getCreateTime() {
//        return createTime;
//    }
//
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    public Timestamp getUpdateTime() {
//        return updateTime;
//    }

    @Getter @Setter
    private String vendorName;

    @Getter @Setter
    private String model;

    @Getter @Setter
    private String serialNumber;

    @Getter @Setter
    private ProjectAXPOJO projectAxaptaId;

    @Getter @Setter
    private String activationId;

    @Getter @Setter
    private String imei;

    @Getter @Setter
    private String mac;
}