package com.indev.tradein.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Описание файла в хранилище
 */
@Entity
@Table(name = "file_description", schema = "tradein"/*, uniqueConstraints = {@UniqueConstraint(columnNames = {"axapta_id"})}*/)
public class FileDescription {
    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator( name = "fileDescriptionSequence", sequenceName = "FILE_DESCRIPTION_SEQUENCE", allocationSize = 1, initialValue = 100 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "fileDescriptionSequence")
    @Column(name = "file_description_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    //<editor-fold desc="ACTIVITY REGION">
    // свойство активности сущности
    private Boolean active = true;

    /**
     * Получить признак активности сущности
     *
     * @return -
     * true - проект активен
     * false - проект заблокирован
     */
    @Column(name = "active", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    //</editor-fold>

    //<editor-fold desc="ENTITY TIMESTAMP REGION">
    // дата и время создания
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    // дата и время последнего обновления
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    /**
     * Получить дату и время создания
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    /**
     * Получить дату и время последнего обновления
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "update_time", nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
//  TODO Разобраться можно ли повесить тригер на уровне БД на автоматическое обновление поле UPDATE при его обновлении
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
    //</editor-fold>

    //<editor-fold desc="JPA EVENTS REGION">
    @PrePersist
    protected void onCreate() {
        Timestamp ts = new Timestamp((new java.util.Date()).getTime());
        this.setCreateTime(ts);
        this.setUpdateTime(ts);
    }

    @PreUpdate
    protected void onUpdate() {
        // обновить update time на текущее время
        this.setUpdateTime(new Timestamp((new java.util.Date()).getTime()));
    }
//</editor-fold>

    // идентификатор теста
    private String testId;

    // относительный путь и имя файла по правилам хранения https://indevpro.atlassian.net/wiki/spaces/TRAD/pages/13729793
    private String fileName;

    // размер файла
    private Long fileSize;

    // идентификатор отчета к которому относится файл
//    private DiagnosticReport diagnosticReport;
    private Long diagnosticReportId;

    @Column(length = 10, nullable = true)
    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    @Column(length = 4096, nullable = true)
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Column(nullable = false )
    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    @Column(name = "report_id", nullable = false )
    public Long getDiagnosticReportId() {
        return diagnosticReportId;
    }

    public void setDiagnosticReportId(Long diagnosticReportId) {
        this.diagnosticReportId = diagnosticReportId;
    }

//    @ManyToOne
//    @JoinColumn( name = "report_id", nullable = false)
//    public DiagnosticReport getDiagnosticReport() {
//        return diagnosticReport;
//    }
//
//    public void setDiagnosticReport(DiagnosticReport diagnosticReport) {
//        this.diagnosticReport = diagnosticReport;
//    }


}
