package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Типы телефонов с которыми работает система, синхронизируется с Axapta
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "phone_type", schema = "tradein"/*, uniqueConstraints = {@UniqueConstraint(columnNames = {"axapta_id"})}*/)
public class PhoneType {

    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator(name = "phoneTypeSequence", sequenceName = "PHONE_TYPE_SEQUENCE", allocationSize = 1, initialValue = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phoneTypeSequence")
    @Column(name = "phone_type_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    //<editor-fold desc="AXAPTA ID">
    // идентификатор по Axapta
    private AXAPTAID axaptaId;

    @JsonIgnore
    public AXAPTAID getAxaptaId() {
        return axaptaId;
    }

    public void setAxaptaId(AXAPTAID axaptaId) {
        this.axaptaId = axaptaId;
    }
    //</editor-fold>

    //<editor-fold desc="ACTIVITY REGION">
    // свойство активности сущности
    private Boolean active = true;

    /**
     * Получить признак активности сущности
     *
     * @return -
     * true - проект активен
     * false - проект заблокирован
     */
    @Column(name = "active", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    //</editor-fold>

    //<editor-fold desc="ENTITY TIMESTAMP REGION">
    // дата и время создания
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    // дата и время последнего обновления
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    /**
     * Получить дату и время создания
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    /**
     * Получить дату и время последнего обновления
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "update_time", nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
//  TODO Разобраться можно ли повесить тригер на уровне БД на автоматическое обновление поле UPDATE при его обновлении
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
    //</editor-fold>

    //<editor-fold desc="JPA EVENTS REGION">
    @PrePersist
    protected void onCreate() {
        Timestamp ts = new Timestamp((new java.util.Date()).getTime());
        this.setCreateTime(ts);
        this.setUpdateTime(ts);
    }

    @PreUpdate
    protected void onUpdate() {
        // обновить update time на текущее время
        this.setUpdateTime(new Timestamp((new java.util.Date()).getTime()));
    }
//</editor-fold>

    //<editor-fold desc="VENDOR MODEL REGION">
    // производитель
    private String vendorName;

    // модель
    private String model;

    @Column(length = 40, nullable = false)
    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @Column(name = "model_caption", length = 64, nullable = true)
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    //</editor-fold>

    //<editor-fold desc="STORAGE SIZE REGION">
    private int storageSizeGb = 0;

    @Column(name = "storage_size_gb", nullable = false, columnDefinition = "INTEGER DEFAULT '0'")
    public int getStorageSizeGb() {
        return storageSizeGb;
    }

    public void setStorageSizeGb(int storageSize) {
        this.storageSizeGb = storageSize;
    }
    //</editor-fold>


    private TestSchema testSchema;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "test_schema_id")
    public TestSchema getTestSchema() {
        return testSchema;
    }

    public void setTestSchema(TestSchema schema) {
        this.testSchema = schema;
    }

    //<editor-fold desc="PRICE RATE REGION">

    /**
     * RATE1
     */
    private String rate1Price = null;

    @Column(nullable = true)
    public String getRate1Price() {
        return rate1Price;
    }

    public void setRate1Price(String rate1Price) {
        this.rate1Price = rate1Price;
    }

    /**
     * RATE2
     */
    private String rate2Price = null;

    @Column(nullable = true)
    public String getRate2Price() {
        return rate2Price;
    }

    public void setRate2Price(String rate2Price) {
        this.rate2Price = rate2Price;
    }

    /**
     * RATE3
     */
    private String rate3Price = null;

    @Column(nullable = true)
    public String getRate3Price() {
        return rate3Price;
    }

    public void setRate3Price(String rate3Price) {
        this.rate3Price = rate3Price;
    }

    /**
     * RATE4
     */
    private String rate4Price = null;

    @Column(nullable = true)
    public String getRate4Price() {
        return rate4Price;
    }

    public void setRate4Price(String rate4Price) {
        this.rate4Price = rate4Price;
    }

    /**
     * RATE5
     */
    private String rate5Price = null;

    @Column(nullable = true)
    public String getRate5Price() {
        return rate5Price;
    }

    public void setRate5Price(String rate5Price) {
        this.rate5Price = rate5Price;
    }

    /**
     * RATE6
     */
    private String rate6Price = null;

    @Column(nullable = true)
    public String getRate6Price() {
        return rate6Price;
    }

    public void setRate6Price(String rate6Price) {
        this.rate6Price = rate6Price;
    }

    /**
     * RATE7
     */
    private String rate7Price = null;

    @Column(nullable = true)
    public String getRate7Price() {
        return rate7Price;
    }

    public void setRate7Price(String rate7Price) {
        this.rate7Price = rate7Price;
    }

    /**
     * RATE8
     */
    private String rate8Price = null;

    @Column(nullable = true)
    public String getRate8Price() {
        return rate8Price;
    }

    public void setRate8Price(String rate8Price) {
        this.rate8Price = rate8Price;
    }

    /**
     * RATE9
     */
    private String rate9Price = null;

    @Column(nullable = true)
    public String getRate9Price() {
        return rate9Price;
    }

    public void setRate9Price(String rate9Price) {
        this.rate9Price = rate9Price;
    }

    /**
     * RATE10
     */
    private String rate10Price = null;

    @Column(nullable = true)
    public String getRate10Price() {
        return rate10Price;
    }

    public void setRate10Price(String rate10Price) {
        this.rate10Price = rate10Price;
    }

    /**
     * RATE11
     */
    private String rate11Price = null;

    @Column(nullable = true)
    public String getRate11Price() {
        return rate11Price;
    }

    public void setRate11Price(String rate11Price) {
        this.rate11Price = rate11Price;
    }

    /**
     * RATE12
     */
    private String rate12Price = null;

    @Column(nullable = true)
    public String getRate12Price() {
        return rate12Price;
    }

    public void setRate12Price(String rate12Price) {
        this.rate12Price = rate12Price;
    }

    /**
     * RATE13
     */
    private String rate13Price = null;

    @Column(nullable = true)
    public String getRate13Price() {
        return rate13Price;
    }

    public void setRate13Price(String rate13Price) {
        this.rate13Price = rate13Price;
    }

    /**
     * RATE14
     */
    private String rate14Price = null;

    @Column(nullable = true)
    public String getRate14Price() {
        return rate14Price;
    }

    public void setRate14Price(String rate14Price) {
        this.rate14Price = rate14Price;
    }

    /**
     * RATE15
     */
    private String rate15Price = null;

    @Column(nullable = true)
    public String getRate15Price() {
        return rate15Price;
    }

    public void setRate15Price(String rate15Price) {
        this.rate15Price = rate15Price;
    }

    /**
     * RATE16
     */
    private String rate16Price = null;

    @Column(nullable = true)
    public String getRate16Price() {
        return rate16Price;
    }

    public void setRate16Price(String rate16Price) {
        this.rate16Price = rate16Price;
    }

    /**
     * RATE17
     */
    private String rate17Price = null;

    @Column(nullable = true)
    public String getRate17Price() {
        return rate17Price;
    }

    public void setRate17Price(String rate17Price) {
        this.rate17Price = rate17Price;
    }

    /**
     * RATE18
     */
    private String rate18Price = null;

    @Column(nullable = true)
    public String getRate18Price() {
        return rate18Price;
    }

    public void setRate18Price(String rate18Price) {
        this.rate18Price = rate18Price;
    }

    /**
     * RATE19
     */
    private String rate19Price = null;

    @Column(nullable = true)
    public String getRate19Price() {
        return rate19Price;
    }

    public void setRate19Price(String rate19Price) {
        this.rate19Price = rate19Price;
    }

    /**
     * RATE20
     */
    private String rate20Price = null;

    @Column(nullable = true)
    public String getRate20Price() {
        return rate20Price;
    }

    public void setRate20Price(String rate20Price) {
        this.rate20Price = rate20Price;
    }

    /**
     * цена для телефона в хорошем состоянии
     */
    private String normalRatePrice = null;

    @Column(nullable = true)
    public String getNormalRatePrice() {
        return normalRatePrice;
    }

    public void setNormalRatePrice(String normalRatePrice) {
        this.normalRatePrice = normalRatePrice;
    }

    /**
     * цена для телефона с незначительными дефектами
     */
    private String minorRatePrice = null;

    @Column(nullable = true)
    public String getMinorRatePrice() {
        return minorRatePrice;
    }

    public void setMinorRatePrice(String minorRatePrice) {
        this.minorRatePrice = minorRatePrice;
    }

    /**
     * цена для телефона со значительными дефектами
     */
    private String majorRatePrice = null;

    @Column(nullable = true)
    public String getMajorRatePrice() {
        return majorRatePrice;
    }

    public void setMajorRatePrice(String majorRatePrice) {
        this.majorRatePrice = majorRatePrice;
    }

    /**
     * цена для телефона с блокирующими дефектами
     */
    private String blockerRatePrice = null;

    @Column(nullable = true)
    public String getBlockerRatePrice() {
        return blockerRatePrice;
    }

    public void setBlockerRatePrice(String blockerRatePrice) {
        this.blockerRatePrice = blockerRatePrice;
    }

    //</editor-fold>

    @Transient
    public boolean isTestSchemaFlag() {
        return (null != getTestSchema());
    }


}

