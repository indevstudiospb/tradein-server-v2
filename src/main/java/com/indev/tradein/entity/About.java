package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.indev.system.MemoryInfo;
import com.indev.system.StorageInfo;
import lombok.Getter;

/**
 * Created by ivan on 17.08.17.
 */
@JsonRootName("About")
public class About {


    @Getter
    private String serviceVersion = "UNDEFINED";

    @Getter
    private String apkVersion = "UNDEFINED";

    @Getter(onMethod=@__({@JsonIgnore}))
    private String apiVersion = "UNDEFINED";

    @Getter(onMethod=@__({@JsonIgnore}))
    private final String path;

    @Getter(onMethod=@__({@JsonProperty("Storage")}))
    final private StorageInfo storageInfo;

    @Getter(onMethod=@__({@JsonProperty("Memory")}))
    final private MemoryInfo memoryInfo;


    public About(String path, String apiVersion, String apkVersion)
    {
        this.path = path;
        this.apiVersion = this.serviceVersion = apiVersion;
        this.apkVersion = apkVersion;
        storageInfo = new StorageInfo( path );
        memoryInfo = new MemoryInfo();
    }
}
