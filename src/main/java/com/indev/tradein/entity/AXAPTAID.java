package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
@ToString
@EqualsAndHashCode
public class AXAPTAID {

    private String axaptaId;

    public AXAPTAID()
    {
    }


    public AXAPTAID(String axaptaId)
    {
        if( null == axaptaId )
            this.setAxaptaId(DEFAULT_AXAPTAID);
        else if ( axaptaId.length() != DEFAULT_AXAPTAID.length() )
            throw new IllegalArgumentException("AXAPTAID: illegal axaptaid=" + axaptaId);
        this.setAxaptaId(axaptaId);
    }

    public static final String DEFAULT_AXAPTAID = "0000000000";

    public void setAxaptaId(String axaptaId) {
        if ( axaptaId.length() != DEFAULT_AXAPTAID.length() )
            throw new IllegalArgumentException("AXAPTAID: illegal axaptaid=" + axaptaId);
        this.axaptaId = axaptaId;
    }

    @JsonIgnore
    @Transient
    public boolean isValid()
    {
        return !DEFAULT_AXAPTAID.equals(axaptaId);
    }

    @Column(name = "axapta_id",  unique = true, nullable = false, length = 10)
    public String getAxaptaId() {
        return axaptaId;
    }
}
