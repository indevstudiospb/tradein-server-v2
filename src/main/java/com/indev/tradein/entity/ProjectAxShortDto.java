package com.indev.tradein.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ProjectAxShortDto
{
    private long projectId;

    private String axaptaId;

    private String caption;

    private boolean active;

    private String activationId;

}
