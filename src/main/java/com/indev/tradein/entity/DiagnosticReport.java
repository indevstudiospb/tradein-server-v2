package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Диагностический отчет
 */
@ToString
@Entity
@Table(name = "diagnostic_report", schema = "tradein"/*, uniqueConstraints = {@UniqueConstraint(columnNames = {"axapta_id"})}*/)
public class DiagnosticReport {

    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator(name = "diagnosticReportSequence", sequenceName = "DIAGNOSTIC_REPORT_SEQUENCE", allocationSize = 1, initialValue = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "diagnosticReportSequence")
    @Column(name = "report_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    //<editor-fold desc="AXAPTA ID">
    // идентификатор по Axapta
    private String axaptaId;

    @Column(name = "axapta_id", unique = true, nullable = true, length = 10)
    public String getAxaptaId() {
        return axaptaId;
    }

    public void setAxaptaId(String axaptaId) {
        this.axaptaId = axaptaId;
    }
    //</editor-fold>

    //<editor-fold desc="checkSessionId">
    // Уникальный Идентификатор отчета, присваиваемый отчету сторонней организацией.
    // Требуется для обеспечения бизнес-процесса Форвард-лизинг - они встраивают tradein SDK в свое андроид-приложение
    private String checkSessionId;

    @Column(name = "check_session_id", unique = true, nullable = true)
    public String getCheckSessionId() {
        return checkSessionId;
    }

    public void setCheckSessionId(String value) {
        checkSessionId = value;
    }
    //</editor-fold>

    //<editor-fold desc="login">
    // Логин пользователя, инициировавшего диагностику через веб-портал
    private String login;

    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String value) {
        login = value;
    }
    //</editor-fold>

    //<editor-fold desc="ACTIVITY REGION">
    // свойство активности сущности
    private Boolean active = true;

    /**
     * Получить признак активности сущности
     *
     * @return -
     * true - проект активен
     * false - проект заблокирован
     */
    @Column(name = "active", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    //</editor-fold>

    //<editor-fold desc="ENTITY TIMESTAMP REGION">
    // дата и время создания
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    // дата и время последнего обновления
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    /**
     * Получить дату и время создания
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    /**
     * Получить дату и время последнего обновления
     *
     * @return
     */
//  TODO Разобраться можно ли повесить тригер на уровне БД на автоматическое обновление поле UPDATE при его обновлении
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "update_time", nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
    //</editor-fold>

    //<editor-fold desc="JPA EVENTS REGION">
//    @PrePersist
    protected void onCreate() {
        Timestamp ts = new Timestamp((new java.util.Date()).getTime());
//        this.setCreateTime(ts);
        this.setUpdateTime(ts);
    }

    @PreUpdate
    protected void onUpdate() {
        // обновить update time на текущее время
        this.setUpdateTime(new Timestamp((new java.util.Date()).getTime()));
    }
    //</editor-fold>

    // проект к которому относится отчет
    private ProjectAX projectAxapta = new ProjectAX();

    // диагностируемый телефон
    private Phone phone = new Phone();

    // отчет диагностического теста
    private String jsonPhoneDiagnosticReport;

    // перечень файлов относящихся к тесту
    private List<FileDescription> fileDescription = new ArrayList<>();

    // перечень ключей относящихся к тесту
    private List<DiagnosticReportStructure> diagnosticReportStructure = new ArrayList<>();

    // получить проект к которому привязан планшет
    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id")
    public ProjectAX getProjectAxapta() {
        return projectAxapta;
    }

   /**
     * Отчет диагностики смартфона.
     *
     * @return - Отчет диагностики смартфонв в формате JSON.
     * Допускается значение null.
     */
    @Column(name = "phone_diagnostic_reprot", length = 32768, nullable = true)
    @JsonIgnore
    public String getJsonPhoneDiagnosticReport() {
        return jsonPhoneDiagnosticReport;
    }

    public void setJsonPhoneDiagnosticReport(String jsonPhoneDiagnosticReport) {
        this.jsonPhoneDiagnosticReport = jsonPhoneDiagnosticReport;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "diagnosticReportId", cascade = CascadeType.ALL)
    public List<FileDescription> getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(List<FileDescription> fileDescription) {
        this.fileDescription = fileDescription;
    }

    public void setProjectAxapta(ProjectAX projectAxapta) {
        this.projectAxapta = projectAxapta;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "phone_id", nullable = false)
    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "diagnosticReport", cascade = CascadeType.ALL)
    public List<DiagnosticReportStructure> getDiagnosticReportStructure() {
        return diagnosticReportStructure;
    }

    public void setDiagnosticReportStructure(List<DiagnosticReportStructure> diagnosticReportStructure) {
        this.diagnosticReportStructure = diagnosticReportStructure;
    }

    //<editor-fold desc="QUALITY STATE">
    private QUALITY qualityRate = QUALITY.unknown;

    @Column(nullable = false, length = 20, columnDefinition = "VARCHAR(20) DEFAULT 'UNKNOWN'")
    public String getQualityRate() {
        return qualityRate.toString();
    }

    @Transient
    @JsonIgnore
    public QUALITY getQualityRateEx() {
        return qualityRate;
    }

    public void setQualityRate(String qualityRate) {
        this.qualityRate = QUALITY.valueOf(qualityRate);
    }

    public void setQualityRate(QUALITY qualityRate) {
        this.qualityRate = qualityRate;
    }
    //</editor-fold>
}

    

