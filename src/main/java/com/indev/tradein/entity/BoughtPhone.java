package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Выкупленный телефон.
 */

@ToString
@Entity
@Table(name = "bought_phone", schema = "tradein", uniqueConstraints = {@UniqueConstraint(columnNames = {"imei1", "project_id", "transaction_time", "phone_type_id"})})
public class BoughtPhone {

    public BoughtPhone()
    {

    }

    public BoughtPhone(Phone phone) {
        this.serialNumber = phone.getSerialNumber();
        this.modelCaption = phone.getModelCaption();
        this.imei1 = phone.getImei1();
        this.imei2 = phone.getImei2();
        //this.phone = phone;
        this.phoneId = phone.getId();
        this.phoneType = phone.getPhoneType();
        this.rate1Price = phone.getPhoneType().getRate1Price();
        this.rate2Price = phone.getPhoneType().getRate2Price();
        this.rate3Price = phone.getPhoneType().getRate3Price();
        this.rate4Price = phone.getPhoneType().getRate4Price();
        this.rate5Price = phone.getPhoneType().getRate5Price();
        this.rate6Price = phone.getPhoneType().getRate6Price();
        this.rate7Price = phone.getPhoneType().getRate7Price();
        this.rate8Price = phone.getPhoneType().getRate8Price();
        this.rate9Price = phone.getPhoneType().getRate9Price();
        this.rate10Price = phone.getPhoneType().getRate10Price();
        this.rate11Price = phone.getPhoneType().getRate11Price();
        this.rate2Price = phone.getPhoneType().getRate2Price();
        this.rate13Price = phone.getPhoneType().getRate13Price();
        this.rate14Price = phone.getPhoneType().getRate14Price();
        this.rate15Price = phone.getPhoneType().getRate15Price();
        this.rate16Price = phone.getPhoneType().getRate16Price();
        this.rate17Price = phone.getPhoneType().getRate17Price();
        this.rate18Price = phone.getPhoneType().getRate18Price();
        this.rate19Price = phone.getPhoneType().getRate19Price();
        this.rate20Price = phone.getPhoneType().getRate20Price();
        this.blockerRatePrice = phone.getPhoneType().getBlockerRatePrice();
        this.majorRatePrice = phone.getPhoneType().getMajorRatePrice();
        this.minorRatePrice = phone.getPhoneType().getMinorRatePrice();
        this.normalRatePrice = phone.getPhoneType().getNormalRatePrice();
        this.qualityRate = QUALITY.valueOf(phone.getQualityRate());
        this.storageSizeGb = phone.getStorageSizeGb();
        this.projectAxapta = phone.getProjectAxapta();
        // TODO сделать заполнение текущего времени
        // this.transactionTime
        // TODO подумать что делать с axaptaid  - похоже ничего делать не надо
        // this.axaptaId
    }

    //<editor-fold desc="STORAGE SIZE REGION">
    private int storageSizeGb=0;

    @Column(name = "storage_size_gb", nullable = false, columnDefinition = "INTEGER DEFAULT '0'")
    public int getStorageSizeGb() {
        return storageSizeGb;
    }

    public void setStorageSizeGb(int storageSize) {
        this.storageSizeGb = storageSize;
    }
    //</editor-fold>
    
    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator( name = "boughtPhoneSequence", sequenceName = "BOUGHT_PHONE_SEQUENCE", allocationSize = 1, initialValue = 100 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "boughtPhoneSequence")
    @Column(name = "bought_phone_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    //<editor-fold desc="AXAPTA ID">
    // идентификатор по Axapta
    private String axaptaId;

    @Column(name = "axapta_id", unique = true, nullable = true, length = 10)
    public String getAxaptaId() {
        return axaptaId;
    }

    public void setAxaptaId(String axaptaId) {
        this.axaptaId = axaptaId;
    }
    //</editor-fold>

    //<editor-fold desc="ACTIVITY REGION">
    // свойство активности сущности
    private Boolean active = true;

    /**
     * Получить признак активности сущности
     *
     * @return -
     * true - проект активен
     * false - проект заблокирован
     */
    @Column(name = "active", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    //</editor-fold>

    //<editor-fold desc="ENTITY TIMESTAMP REGION">
    // дата и время создания
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    // дата и время последнего обновления
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    /**
     * Получить дату и время создания
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
    public Timestamp getCreateTime() {
        return createTime;
    }

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    /**
     * Получить дату и время последнего обновления
     *
     * @return
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    @Basic(optional = true)
    @Column(name = "update_time", nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
//  TODO Разобраться можно ли повесить тригер на уровне БД на автоматическое обновление поле UPDATE при его обновлении
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
    //</editor-fold>

    //<editor-fold desc="JPA EVENTS REGION">
    @PrePersist
    protected void onCreate() {
        Timestamp ts = new Timestamp((new java.util.Date()).getTime());
        this.setCreateTime(ts);
        this.setUpdateTime(ts);
    }

    @PreUpdate
    protected void onUpdate() {
        // обновить update time на текущее время
        this.setUpdateTime(new Timestamp((new java.util.Date()).getTime()));
    }
//</editor-fold>

    private String serialNumber;

    private String modelCaption;

    private String imei1;

    private String imei2;

    // проект к которому относится планшет
    private PhoneType phoneType;

    // получить проект к которому привязан планшет
    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "phone_type_id", nullable = true)
    public PhoneType getPhoneType() {
        return phoneType;
    }

    // установить проект к которому привязан планшет
    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }
    
    @Column(length = 40, nullable = false)
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Column(length = 64, nullable = true)
    public String getModelCaption() {
        return modelCaption;
    }

    public void setModelCaption(String modelCaption) {
        this.modelCaption = modelCaption;
    }

    @Column(name="imei1", length = 60, unique = false, nullable = false)
    public String getImei1() {
        return imei1;
    }

    public void setImei1(String imei1) {
        this.imei1 = imei1;
    }

    @Column(name="imei2", length = 40, nullable = true)
    public String getImei2() {
        return imei2;
    }

    public void setImei2(String imei2) {
        this.imei2 = imei2;
    }

    // дата и время транзакции
    private Timestamp transactionTime; // = new Timestamp((new java.util.Date()).getTime());

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    @Column(name="transaction_time", nullable = false)
    public Timestamp getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Timestamp transactionTime) {
        this.transactionTime = transactionTime;
    }

    //<editor-fold desc="PHONE">
    // диагностируемый телефон
    private long phoneId;

    public long getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(long phoneId) {
        this.phoneId = phoneId;
    }
    //</editor-fold>

    //<editor-fold desc="PRICE RATE REGION">

    /**
     * RATE1
     */
    private String rate1Price = null;

    @Column(nullable = true)
    public String getRate1Price() {
        return rate1Price;
    }

    public void setRate1Price(String rate1Price) {
        this.rate1Price = rate1Price;
    }

    /**
     * RATE2
     */
    private String rate2Price = null;

    @Column(nullable = true)
    public String getRate2Price() {
        return rate2Price;
    }

    public void setRate2Price(String rate2Price) {
        this.rate2Price = rate2Price;
    }

    /**
     * RATE3
     */
    private String rate3Price = null;

    @Column(nullable = true)
    public String getRate3Price() {
        return rate3Price;
    }

    public void setRate3Price(String rate3Price) {
        this.rate3Price = rate3Price;
    }

    /**
     * RATE4
     */
    private String rate4Price = null;

    @Column(nullable = true)
    public String getRate4Price() {
        return rate4Price;
    }

    public void setRate4Price(String rate4Price) {
        this.rate4Price = rate4Price;
    }

    /**
     * RATE5
     */
    private String rate5Price = null;

    @Column(nullable = true)
    public String getRate5Price() {
        return rate5Price;
    }

    public void setRate5Price(String rate5Price) {
        this.rate5Price = rate5Price;
    }

    /**
     * RATE6
     */
    private String rate6Price = null;

    @Column(nullable = true)
    public String getRate6Price() {
        return rate6Price;
    }

    public void setRate6Price(String rate6Price) {
        this.rate6Price = rate6Price;
    }

    /**
     * RATE7
     */
    private String rate7Price = null;

    @Column(nullable = true)
    public String getRate7Price() {
        return rate7Price;
    }

    public void setRate7Price(String rate7Price) {
        this.rate7Price = rate7Price;
    }

    /**
     * RATE8
     */
    private String rate8Price = null;

    @Column(nullable = true)
    public String getRate8Price() {
        return rate8Price;
    }

    public void setRate8Price(String rate8Price) {
        this.rate8Price = rate8Price;
    }

    /**
     * RATE9
     */
    private String rate9Price = null;

    @Column(nullable = true)
    public String getRate9Price() {
        return rate9Price;
    }

    public void setRate9Price(String rate9Price) {
        this.rate9Price = rate9Price;
    }

    /**
     * RATE10
     */
    private String rate10Price = null;

    @Column(nullable = true)
    public String getRate10Price() {
        return rate10Price;
    }

    public void setRate10Price(String rate10Price) {
        this.rate10Price = rate10Price;
    }

    /**
     * RATE11
     */
    private String rate11Price = null;

    @Column(nullable = true)
    public String getRate11Price() {
        return rate11Price;
    }

    public void setRate11Price(String rate11Price) {
        this.rate11Price = rate11Price;
    }

    /**
     * RATE12
     */
    private String rate12Price = null;

    @Column(nullable = true)
    public String getRate12Price() {
        return rate12Price;
    }

    public void setRate12Price(String rate12Price) {
        this.rate12Price = rate12Price;
    }

    /**
     * RATE13
     */
    private String rate13Price = null;

    @Column(nullable = true)
    public String getRate13Price() {
        return rate13Price;
    }

    public void setRate13Price(String rate13Price) {
        this.rate13Price = rate13Price;
    }

    /**
     * RATE14
     */
    private String rate14Price = null;

    @Column(nullable = true)
    public String getRate14Price() {
        return rate14Price;
    }

    public void setRate14Price(String rate14Price) {
        this.rate14Price = rate14Price;
    }

    /**
     * RATE15
     */
    private String rate15Price = null;

    @Column(nullable = true)
    public String getRate15Price() {
        return rate15Price;
    }

    public void setRate15Price(String rate15Price) {
        this.rate15Price = rate15Price;
    }

    /**
     * RATE16
     */
    private String rate16Price = null;

    @Column(nullable = true)
    public String getRate16Price() {
        return rate16Price;
    }

    public void setRate16Price(String rate16Price) {
        this.rate16Price = rate16Price;
    }

    /**
     * RATE17
     */
    private String rate17Price = null;

    @Column(nullable = true)
    public String getRate17Price() {
        return rate17Price;
    }

    public void setRate17Price(String rate17Price) {
        this.rate17Price = rate17Price;
    }

    /**
     * RATE18
     */
    private String rate18Price = null;

    @Column(nullable = true)
    public String getRate18Price() {
        return rate18Price;
    }

    public void setRate18Price(String rate18Price) {
        this.rate18Price = rate18Price;
    }

    /**
     * RATE19
     */
    private String rate19Price = null;

    @Column(nullable = true)
    public String getRate19Price() {
        return rate19Price;
    }

    public void setRate19Price(String rate19Price) {
        this.rate19Price = rate19Price;
    }

    /**
     * RATE20
     */
    private String rate20Price = null;

    @Column(nullable = true)
    public String getRate20Price() {
        return rate20Price;
    }

    public void setRate20Price(String rate20Price) {
        this.rate20Price = rate20Price;
    }

    /**
     * цена для телефона в хорошем состоянии
     */
    private String normalRatePrice = null;

    @Column(nullable = true)
    public String getNormalRatePrice() {
        return normalRatePrice;
    }

    public void setNormalRatePrice(String normalRatePrice) {
        this.normalRatePrice = normalRatePrice;
    }

    /**
     * цена для телефона с незначительными дефектами
     */
    private String minorRatePrice = null;

    @Column(nullable = true)
    public String getMinorRatePrice() {
        return minorRatePrice;
    }

    public void setMinorRatePrice(String minorRatePrice) {
        this.minorRatePrice = minorRatePrice;
    }

    /**
     * цена для телефона со значительными дефектами
     */
    private String majorRatePrice = null;

    @Column(nullable = true)
    public String getMajorRatePrice() {
        return majorRatePrice;
    }

    public void setMajorRatePrice(String majorRatePrice) {
        this.majorRatePrice = majorRatePrice;
    }

    /**
     * цена для телефона с блокирующими дефектами
     */
    private String blockerRatePrice = null;

    @Column(nullable = true)
    public String getBlockerRatePrice() {
        return blockerRatePrice;
    }

    public void setBlockerRatePrice(String blockerRatePrice) {
        this.blockerRatePrice = blockerRatePrice;
    }

    //</editor-fold>
    
    //<editor-fold desc="PRICE">
    private String acceptedPrice;

    @Column(name="accepted_price", nullable = false)
    public String getAcceptedPrice() {
        return acceptedPrice;
    }

    public void setacceptedPrice(String price) {
        this.acceptedPrice = price;
    }
    //</editor-fold>

    //<editor-fold desc="QUALITY STATE">
    private QUALITY qualityRate = QUALITY.unknown;

    @Column(nullable = false, length = 20, columnDefinition = "VARCHAR(20) DEFAULT 'UNKNOWN'")
    public String getQualityRate() {
        return qualityRate.toString();
    }

    public void setQualityRate(String qualityRate) {
        this.qualityRate = QUALITY.valueOf(qualityRate);
    }

    public void setQualityRate(QUALITY qualityRate) {
        this.qualityRate = qualityRate;
    }
    //</editor-fold>

    //<editor-fold desc="PROJECT">
    // проект к которому относится телефон
    private ProjectAX projectAxapta = new ProjectAX();

    // получить проект к которому привязан планшет
    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", nullable = true)
    public ProjectAX getProjectAxapta() {
        return projectAxapta;
    }

    public void setProjectAxapta(ProjectAX projectAxapta) {
        this.projectAxapta = projectAxapta;
    }
    //</editor-fold>

    @Setter
    @Getter(onMethod=@__({@JsonIgnore}))
    private Boolean delivery;


}
