package com.indev.tradein.entity;

import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ivan on 31.08.17.
 */

@Entity
@Table(name = "phone", schema = "tradein", uniqueConstraints = {@UniqueConstraint(columnNames = {"imei1", "project_id", "bought_phone_id", "phone_type_id"})})
@ToString
public class Phone {

    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator(name = "phoneSequence", sequenceName = "PHONE_SEQUENCE", allocationSize = 1, initialValue = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phoneSequence")
    @Column(name = "phone_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    //<editor-fold desc="ACTIVITY REGION">
    // свойство активности сущности
    private Boolean active = true;

    /**
     * Получить признак активности сущности
     *
     * @return -
     * true - проект активен
     * false - проект заблокирован
     */
    @Column(name = "active", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    //</editor-fold>

    //<editor-fold desc="ENTITY TIMESTAMP REGION">
    // дата и время создания
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    // дата и время последнего обновления
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    /**
     * Получить дату и время создания
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
    public Timestamp getCreateTime() {
        return createTime;
    }

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    /**
     * Получить дату и время последнего обновления
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "update_time", nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
//  TODO Разобраться можно ли повесить тригер на уровне БД на автоматическое обновление поле UPDATE при его обновлении
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
    //</editor-fold>

    //<editor-fold desc="JPA EVENTS REGION">
    @PrePersist
    protected void onCreate() {
        Timestamp ts = new Timestamp((new java.util.Date()).getTime());
        this.setCreateTime(ts);
        this.setUpdateTime(ts);
    }

    @PreUpdate
    protected void onUpdate() {
        // обновить update time на текущее время
        this.setUpdateTime(new Timestamp((new java.util.Date()).getTime()));
    }
//</editor-fold>

    //<editor-fold desc="STORAGE SIZE REGION">
    private int storageSizeGb = 0;

    @Column(name = "storage_size_gb", nullable = false, columnDefinition = "INTEGER DEFAULT '0'")
    public int getStorageSizeGb() {
        return storageSizeGb;
    }

    public void setStorageSizeGb(int storageSize) {
        this.storageSizeGb = storageSize;
    }
    //</editor-fold>

    private String serialNumber;

    private String modelCaption;

    private String imei1;

    private String imei2;

    // проект к которому относится планшет
    private PhoneType phoneType;

    // получить проект к которому привязан планшет
    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "phone_type_id", nullable = true)
    public PhoneType getPhoneType() {
        return phoneType;
    }

    // установить проект к которому привязан планшет
    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }

    // дата и время производства
    private Timestamp productionTime; // = new Timestamp((new java.util.Date()).getTime());

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    @Column(name = "production_time", nullable = false)
    public Timestamp getProductionTime() {
        return productionTime;
    }

    public void setProductionTime(Timestamp productionTime) {
        this.productionTime = productionTime;
    }

    @Column(length = 40, nullable = false)
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Column(length = 64, nullable = true)
    public String getModelCaption() {
        return modelCaption;
    }

    public void setModelCaption(String modelCaption) {
        this.modelCaption = modelCaption;
    }

    @Column(name = "imei1", length = 40, unique = false, nullable = false )
    public String getImei1() {
        return imei1;
    }

    public void setImei1(String imei1) {
        this.imei1 = imei1;
    }

    @Column(name = "imei2", length = 40, nullable = true)
    public String getImei2() {
        return imei2;
    }

    public void setImei2(String imei2) {
        this.imei2 = imei2;
    }

    //<editor-fold desc="QUALITY STATE">
    private QUALITY qualityRate = QUALITY.unknown;

    @Column(nullable = false, length = 20, columnDefinition = "VARCHAR(20) DEFAULT 'UNKNOWN'")
    public String getQualityRate() {
        return qualityRate.toString();
    }

    public void setQualityRate(String qualityRate) {
        this.qualityRate = QUALITY.valueOf(qualityRate);
    }

    public void setQualityRate(QUALITY qualityRate) {
        this.qualityRate = qualityRate;
    }
    //</editor-fold>

    //<editor-fold desc="PROJECT">
    // проект к которому относится телефон
    private ProjectAX projectAxapta = new ProjectAX();

    // получить проект к которому привязан планшет
    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", nullable = true)
    public ProjectAX getProjectAxapta() {
        return projectAxapta;
    }

    public void setProjectAxapta(ProjectAX projectAxapta) {
        this.projectAxapta = projectAxapta;
    }
    //</editor-fold>

    //<editor-fold desc="BOUGHT_PHONE">
    // ссылка на выкупленный телефон
    private BoughtPhone boughtPhone = null;

    @OneToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "bought_phone_id")
    public BoughtPhone getBoughtPhone() {
        return boughtPhone;
    }

    public void setBoughtPhone(BoughtPhone boughtPhone) {
        this.boughtPhone = boughtPhone;
    }
    //</editor-fold>

    // идентификатор диагностического отчета
    private long diagnosticReportId;

    @Column(name = "diagnostic_report_id", nullable = false, unique = true)
    public long getDiagnosticReportId() {
        return diagnosticReportId;
    }

    public void setDiagnosticReportId(long diagnosticReportId) {
        this.diagnosticReportId = diagnosticReportId;
    }

}
