package com.indev.tradein.entity;

import lombok.ToString;

import java.sql.Timestamp;

/**
 * Выкупленный телефон. Краткая структура.
 */

@ToString
public class BoughtPhoneBrief {

    public BoughtPhoneBrief(final BoughtPhone boughtPhone) {
        if( null == boughtPhone )
            throw new NullPointerException("BoughtPhoneBrief.BoughtPhoneBrief: boughtPhone");
        id = boughtPhone.getId();
        axaptaId = boughtPhone.getAxaptaId();
        createTime = boughtPhone.getCreateTime();
        serialNumber = boughtPhone.getSerialNumber();
        imei1 = boughtPhone.getImei1();
        imei2 = boughtPhone.getImei2();
        phoneType = boughtPhone.getPhoneType();
        transactionTime = boughtPhone.getTransactionTime();
        acceptedPrice = boughtPhone.getAcceptedPrice();
        qualityRate = QUALITY.valueOf(boughtPhone.getQualityRate());
        storageSizeGb = boughtPhone.getStorageSizeGb();
    }

    // идентификатор по базе
    public long id;

    public String axaptaId;

    // дата и время создания
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    public Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    public String serialNumber;

    public String imei1;

    public String imei2;

    // проект к которому относится планшет
    public PhoneType phoneType;

    // размер внутреннего хранилища телефона
    public int storageSizeGb;

    // дата и время транзакции
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    public Timestamp transactionTime; // = new Timestamp((new java.util.Date()).getTime());

    public String acceptedPrice;

    public QUALITY qualityRate = QUALITY.unknown;
    
}
