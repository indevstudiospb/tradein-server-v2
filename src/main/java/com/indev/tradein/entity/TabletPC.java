package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * Created by ivan on 22.08.17.
 */
@ToString
@Entity
@Table(name = "tabletpc", schema = "tradein"/*, uniqueConstraints = {@UniqueConstraint(columnNames = {"axapta_id"})}*/)
@Deprecated
public class TabletPC {

    public TabletPC()
    {

    }

    public TabletPC(TabletPCPOJO tabletPCPOJO) {
        id = tabletPCPOJO.getId();
        axaptaId = tabletPCPOJO.getAxaptaId();
        active = tabletPCPOJO.getActive();
        createTime = tabletPCPOJO.getCreateTime();
        updateTime = tabletPCPOJO.getUpdateTime();
        vendorName = tabletPCPOJO.getVendorName();
        model = tabletPCPOJO.getModel();
        serialNumber = tabletPCPOJO.getSerialNumber();
        activationId = tabletPCPOJO.getActivationId();
        imei = tabletPCPOJO.getImei();
        mac = tabletPCPOJO.getMac();

        if (null != tabletPCPOJO.getProjectAxaptaId())
            projectAxaptaId = new ProjectAX(tabletPCPOJO.getProjectAxaptaId());
    }

    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator( name = "tabletpcSequence", sequenceName = "TABLETPC_SEQUENCE", allocationSize = 1, initialValue = 100 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "tabletpcSequence")
    @Column(name = "tablet_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    //<editor-fold desc="AXAPTA ID">
    // идентификатор по Axapta
    private AXAPTAID axaptaId;

    @JsonIgnore
    public AXAPTAID getAxaptaId() {
        return axaptaId;
    }

    public void setAxaptaId(AXAPTAID axaptaId) {
        this.axaptaId = axaptaId;
    }
    //</editor-fold>

    //<editor-fold desc="ACTIVITY REGION">
    // свойство активности сущности
    private Boolean active = true;

    /**
     * Получить признак активности сущности
     *
     * @return -
     * true - проект активен
     * false - проект заблокирован
     */
    @Column(name = "active", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    //</editor-fold>

    //<editor-fold desc="ENTITY TIMESTAMP REGION">
    // дата и время создания
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    // дата и время последнего обновления
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    /**
     * Получить дату и время создания
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    /**
     * Получить дату и время последнего обновления
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "update_time", nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  @Temporal(TemporalType.TIMESTAMP)
//  TODO Разобраться можно ли повесить тригер на уровне БД на автоматическое обновление поле UPDATE при его обновлении
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
    //</editor-fold>

    //<editor-fold desc="JPA EVENTS REGION">
    @PrePersist
    protected void onCreate() {
        Timestamp ts = new Timestamp((new java.util.Date()).getTime());
        this.setCreateTime(ts);
        this.setUpdateTime(ts);
    }

    @PreUpdate
    protected void onUpdate() {
        // обновить update time на текущее время
        this.setUpdateTime(new Timestamp((new java.util.Date()).getTime()));
    }
//</editor-fold>

    //<editor-fold desc="VENDOR MODEL SERIALNUMBER REGION">
    // производитель
    private String vendorName;

    // модель
    private String model;

    // серийный номер
    private String serialNumber;

    @Column(length = 40, nullable = false)
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Column(length = 40, nullable = false)
    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @Column(name="model_caption", length = 64, nullable = true)
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    //</editor-fold>

    // проект к которому относится планшет
    private ProjectAX projectAxaptaId;

    // дата и время создания

    // получить проект к которому привязан планшет
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", nullable = true)
    public ProjectAX getProjectAxaptaId() {
        return projectAxaptaId;
    }

    // установить проект к которому привязан планшет
    public void setProjectAxaptaId(ProjectAX projectAX) {
        this.projectAxaptaId = projectAX;
    }

    //<editor-fold desc="UUID">
    private String activationId;

    // активационный код планшета
    @Column(name = "activation_id", unique = true, nullable = false )
    public String getActivationId() {
        return activationId;
    }

    public void setActivationId(String activationId) {
        this.activationId = activationId;
    }
    //</editor-fold>

    private String imei;

    private String mac;

    @Column(name = "imei" )
    public String getImei() {
        return imei;
    }


    @Column(name = "mac" )
    public String getMac() {
        return mac;
    }


    public void setImei(String imei) {
        this.imei = imei;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}