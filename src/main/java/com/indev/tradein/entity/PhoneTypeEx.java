package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * Типы телефонов с которыми работает система, синхронизируется с Axapta
 */
@ToString
public class PhoneTypeEx {
    // идентификатор по базе
    @Getter
    @Setter
    private long id;

    @Setter @Getter(onMethod=@__({@JsonUnwrapped}))
    private AXAPTAID axaptaId;

    @Getter
    @Setter
    private Boolean active = true;
    
    @Getter(onMethod=@__({@JsonUnwrapped}))
    @Setter
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    @Getter
    @Setter
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    // производитель
    @Getter
    @Setter
    private String vendorName;

    // модель
    @Getter
    @Setter
    private String model;

    // серийный номер
    @Getter
    @Setter
    private int storageSizeGb;

    @Getter
    @Setter
    private long testSchemaId;

    @Getter
    @Setter
    private String rate1Price = null;

    @Getter
    @Setter
    private String rate2Price = null;

    @Getter
    @Setter
    private String rate3Price = null;

    @Getter
    @Setter
    private String rate4Price = null;

    @Getter
    @Setter
    private String rate5Price = null;

    @Getter
    @Setter
    private String rate6Price = null;

    @Getter
    @Setter
    private String rate7Price = null;

    @Getter
    @Setter
    private String rate8Price = null;

    @Getter
    @Setter
    private String rate9Price = null;

    @Getter
    @Setter
    private String rate10Price = null;

    @Getter
    @Setter
    private String rate11Price = null;

    @Getter
    @Setter
    private String rate12Price = null;

    @Getter
    @Setter
    private String rate13Price = null;

    @Getter
    @Setter
    private String rate14Price = null;

    @Getter
    @Setter
    private String rate15Price = null;

    @Getter
    @Setter
    private String rate16Price = null;

    @Getter
    @Setter
    private String rate17Price = null;

    @Getter
    @Setter
    private String rate18Price = null;

    @Getter
    @Setter
    private String rate19Price = null;

    @Getter
    @Setter
    private String rate20Price = null;

    // цена для телефона в хорошем состоянии
    @Getter
    @Setter
    private String normalRatePrice = null;

    // цена для телефона с незначительными дефектами
    @Getter
    @Setter
    private String minorRatePrice = null;

    // цена для телефона со значительными дефектами
    @Getter
    @Setter
    private String majorRatePrice = null;

    // цена для телефона с блокирующими дефектами
    @Getter
    @Setter
    private String blockerRatePrice = null;

    public PhoneTypeEx()
    {

    }

    public PhoneTypeEx(PhoneType phType) {
        this.id = phType.getId();
        this.setAxaptaId(phType.getAxaptaId());
        this.active = phType.getActive();
        this.createTime = phType.getCreateTime();
        this.updateTime = phType.getUpdateTime();
        // производитель
        this.vendorName = phType.getVendorName();
        // модель
        this.model = phType.getModel();
        // серийный номер
        this.storageSizeGb = phType.getStorageSizeGb();
        this.testSchemaId = ( null != phType.getTestSchema()) ? phType.getTestSchema().getId() : 0;
        this.rate1Price = phType.getRate1Price();
        this.rate2Price = phType.getRate2Price();
        this.rate3Price = phType.getRate3Price();
        this.rate4Price = phType.getRate4Price();
        this.rate5Price = phType.getRate5Price();
        this.rate6Price = phType.getRate6Price();
        this.rate7Price = phType.getRate7Price();
        this.rate8Price = phType.getRate8Price();
        this.rate9Price = phType.getRate9Price();
        this.rate10Price = phType.getRate10Price();
        this.rate11Price = phType.getRate11Price();
        this.rate12Price = phType.getRate12Price();
        this.rate13Price = phType.getRate13Price();
        this.rate14Price = phType.getRate14Price();
        this.rate15Price = phType.getRate15Price();
        this.rate16Price = phType.getRate16Price();
        this.rate17Price = phType.getRate17Price();
        this.rate18Price = phType.getRate18Price();
        this.rate19Price = phType.getRate19Price();
        this.rate20Price = phType.getRate20Price();
        this.normalRatePrice = phType.getNormalRatePrice();
        this.minorRatePrice = phType.getMinorRatePrice();
        this.majorRatePrice = phType.getMajorRatePrice();
        this.blockerRatePrice = phType.getBlockerRatePrice();
    }

}

