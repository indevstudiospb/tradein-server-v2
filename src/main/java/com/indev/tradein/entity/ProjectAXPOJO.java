package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.Set;


/**
 * Created by ivan on 22.08.17.
 */
@ToString
@JsonIgnoreProperties(ignoreUnknown=true)
public class ProjectAXPOJO {
    @Getter @Setter
    private long id;

    @Getter @Setter
    private AXAPTAID axaptaId;

    @Getter @Setter
    private Boolean active = true;

    @Getter @Setter
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    @Getter @Setter
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    @Getter @Setter
    private String activationId;

    @Getter @Setter
    private String logoFile;

    @Getter @Setter
    private String storagePath;

    @Getter @Setter
    private String caption;

    @Getter @Setter
    private Set<PhoneType> phoneTypes;
}
