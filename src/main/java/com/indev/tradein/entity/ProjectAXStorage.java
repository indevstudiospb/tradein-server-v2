package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;
import lombok.Setter;


/**
 * Структура команды для смены хранилища у проекта
 */

public class ProjectAXStorage {

    public ProjectAXStorage(ProjectAX projectAX)
    {
        if( null == projectAX )
            throw new NullPointerException("ProjectAX");
        id = projectAX.getId();
        axaptaId = projectAX.getAxaptaId();
        storagePath = projectAX.getStoragePath();
    }

    // идентификатор
    @Setter @Getter
    private long id;

    // идентификатор Axapta
    @Setter @Getter(onMethod=@__({@JsonUnwrapped}))
    private AXAPTAID axaptaId;

    // путь хранения данных проекта, если путь не задан или null, то используем исходное хранилище
    @Setter @Getter
    private String storagePath;
}
