package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@ToString
@Entity
@Table(name = "resource", schema = "tradein"/*, uniqueConstraints = {@UniqueConstraint(columnNames = {"axapta_id"})}*/)
@JsonIgnoreProperties(ignoreUnknown=true)
@NoArgsConstructor
public class ProjectResource {

    public ProjectResource(String resource) {
        this.value = resource;
    }


    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator( name = "resourceSequence", sequenceName = "RESOURCE_SEQUENCE", allocationSize = 1, initialValue = 100 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "resourceSequence")
    @Column(name = "resource_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    private String value;

    @Column(name = "value", length = 32768, nullable = false )
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
