package com.indev.tradein.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;


/**
 * Created by ivan on 22.08.17.
 */
@ToString
@Entity
@Table(name = "project", schema = "tradein"/*, uniqueConstraints = {@UniqueConstraint(columnNames = {"axapta_id"})}*/)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ProjectAX {

    public ProjectAX()
    {
        
    }

    public ProjectAX(ProjectAXPOJO projectAxaptaId) {
        id = projectAxaptaId.getId();
        axaptaId = projectAxaptaId.getAxaptaId();
        active = projectAxaptaId.getActive();
        createTime = projectAxaptaId.getCreateTime();
        updateTime = projectAxaptaId.getUpdateTime();
        caption = projectAxaptaId.getCaption();
        logoFile = projectAxaptaId.getLogoFile();
        activationId = projectAxaptaId.getActivationId();
        phoneTypes = projectAxaptaId.getPhoneTypes();
        storagePath = projectAxaptaId.getStoragePath();
    }

    //<editor-fold desc="ENTITY ID REGION !!! COLUMN NAME">
    // идентификатор по базе
    private long id;

    // получить идентификатор по базе
    @Id
    @SequenceGenerator( name = "projectSequence", sequenceName = "PROJECT_SEQUENCE", allocationSize = 1, initialValue = 100 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "projectSequence")
    @Column(name = "project_id", nullable = false, updatable = false)
    public long getId() {
        return id;
    }

    // установить идентификатор по базе
    public void setId(long id) {
        this.id = id;
    }
    //</editor-fold>

    //<editor-fold desc="AXAPTA ID">
    // идентификатор по Axapta
    private AXAPTAID axaptaId;
    
    @JsonIgnore
    @Column(name = "axapta_id", unique = true, nullable = false, length = 10)
    public AXAPTAID getAxaptaId() {
        return axaptaId;
    }

    public void setAxaptaId(AXAPTAID axaptaId) {
        this.axaptaId = axaptaId;
    }
    //</editor-fold>

    //<editor-fold desc="ACTIVITY REGION">
    // свойство активности сущности
    private Boolean active = true;

    /**
     * Получить признак активности сущности
     *
     * @return -
     * true - проект активен
     * false - проект заблокирован
     */
    @Column(name = "active", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    //</editor-fold>

    //<editor-fold desc="ENTITY TIMESTAMP REGION">
    // дата и время создания
    private Timestamp createTime; // = new Timestamp((new java.util.Date()).getTime());

    // дата и время последнего обновления
    private Timestamp updateTime; // = new Timestamp((new java.util.Date()).getTime());

    /**
     * Получить дату и время создания
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "create_time", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    /**
     * Получить дату и время последнего обновления
     *
     * @return
     */
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
//    @Basic(optional = true)
    @Column(name = "update_time", nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//  TODO Разобраться можно ли повесить тригер на уровне БД на автоматическое обновление поле UPDATE при его обновлении
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
    //</editor-fold>

    //<editor-fold desc="JPA EVENTS REGION">
    @PrePersist
    protected void onCreate() {
        Timestamp ts = new Timestamp((new java.util.Date()).getTime());
        this.setCreateTime(ts);
        this.setUpdateTime(ts);
    }

    @PreUpdate
    protected void onUpdate() {
        // обновить update time на текущее время
        this.setUpdateTime(new Timestamp((new java.util.Date()).getTime()));
    }
    //</editor-fold>

    //<editor-fold desc="UUID">
    private String activationId;

    // активационный код планшета
    @Column(name = "activation_id", unique = true, nullable = false )
    public String getActivationId() {
        return activationId;
    }

    public void setActivationId(String activationId) {
        this.activationId = activationId;
    }
    //</editor-fold>

    @JsonIgnore
    @Transient
    private static String globalPath;
    
    /**
     * Установить storage по умолчанию
     * @param globalStoragePath
     */
    public static void DefineGlobalPath(String globalStoragePath) {
        if (null == globalStoragePath)
            throw new IllegalArgumentException("globalStoragePath can't be null");

        if (globalStoragePath.isEmpty())
            throw new IllegalArgumentException("globalStoragePath can't be empty");

        globalPath = globalStoragePath;
    }

    // путь к идентификатору логотипа
    private String logoFile;

    // путь хранения данных проекта, если путь не задан или null, то используем исходное хранилище
    private String storagePath;

    /**
     * Получить ссылку на файл логотипа
     *
     * @return - ссылкв на файл логотипа, значение может быть null
     */
    @Column(name = "logo_file", nullable = true)
    public String getLogoFile() {
        return logoFile;
    }

    public void setLogoFile(String logoFile) {
        this.logoFile = logoFile;
    }

    private TestSchema testSchema;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "test_schema_id")
    public TestSchema getTestSchema() {
        return testSchema;
    }

    public void setTestSchema(TestSchema schema) {
        this.testSchema = schema;
    }

    /**
     * Получить путь к хранилищу проекта.
     *
     * @return
     */
    @JsonIgnore
    @Column(name = "storage_path", length = 4096, nullable = true)
    public String getStoragePath() {
        return (null == storagePath || storagePath.isEmpty()) ? ProjectAX.globalPath : storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    //<editor-fold desc="CAPTION">
    private String caption;

    @Column(name = "caption", length = 64, nullable = true)
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
    //</editor-fold>
    
    // Перечень типов телефонов, которые принимаются в рамках проекта.
    // Если список не задан, то в рамках проекта не допускается
    // принимать телефоны.
    private Set<PhoneType> phoneTypes;

    //@JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public Set<PhoneType> getPhoneTypes() {
        return phoneTypes;
    }

    public void setPhoneTypes(Set<PhoneType> acceptedPhoneType) {
        this.phoneTypes = acceptedPhoneType;
    }

    @Transient
    public boolean isTestSchemaFlag()
    {
        return ( null != getTestSchema() );
    }

    // ресурсы проекта
    private ProjectResource resource;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    @JoinColumn(name = "resource_id")
    public ProjectResource getResource() {
        return resource;
    }

    public void setResource(ProjectResource resource) {
        this.resource = resource;
    }

    @Transient
    public boolean isResourceFlag()
    {
        return ( null != getResource() );
    }

}
