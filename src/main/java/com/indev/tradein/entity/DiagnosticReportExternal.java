package com.indev.tradein.entity;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;

/**
 * Диагностический отчет для внешних систем.
 */
public class DiagnosticReportExternal {

    public DiagnosticReportExternal(DiagnosticReport diagnosticReport, String acceptedPrice ) {
        if (null == diagnosticReport)
            throw new NullPointerException("DiagnosticReportExternal.DiagnosticReportExternal: diagnosticReport");
        id = diagnosticReport.getId();
        active = diagnosticReport.getActive();
        createTime = diagnosticReport.getCreateTime();
        updateTime = diagnosticReport.getUpdateTime();
        projectAxapta = diagnosticReport.getProjectAxapta();
        qualityRate = QUALITY.valueOf(diagnosticReport.getQualityRate());
        phone = diagnosticReport.getPhone();
        fileDescription = diagnosticReport.getFileDescription();
        this.acceptedPrice = acceptedPrice;

    }

    // идентификатор по базе
    @Getter
    @Setter
    private long id;

    // свойство активности сущности
    @Getter
    @Setter
    private Boolean active = true;

    // дата и время создания
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    @Getter
    @Setter
    private Timestamp createTime;

    // дата и время последнего обновления
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    @Getter
    @Setter
    private Timestamp updateTime;

    // проект к которому относится отчет
    @Getter
    @Setter
    private ProjectAX projectAxapta;

    // диагностируемый телефон
    @Getter
    @Setter
    private Phone phone;

    // перечень файлов относящихся к тесту
    @Getter
    @Setter
    private List<FileDescription> fileDescription;

    @Getter
    @Setter
    private QUALITY qualityRate = QUALITY.unknown;

    @Getter
    @Setter
    private String acceptedPrice;
}

    

