package com.indev.tradein;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Timestamp;

@Component
public class TimestampCustomSerializer extends StdSerializer<Timestamp> {

    public TimestampCustomSerializer() {
        super(Timestamp.class);
    }

    private DateTimeFormatter formatter = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss.SSSZ");

    @Override
    public void serialize(Timestamp value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        DateTime dt = new DateTime(value, DateTimeZone.UTC);
        gen.writeString(dt.toString(this.formatter));
    }


}
